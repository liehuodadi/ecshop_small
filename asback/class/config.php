<?php
if(!defined('InEmpireBak'))
{
	exit();
}
define('EmpireBakConfig',TRUE);

//Database
$phome_db_dbtype='mysql';
$phome_db_ver='5.0';
$phome_db_server='localhost';
$phome_db_port='';
$phome_db_username='root';
$phome_db_password='123456';
$phome_db_dbname='';
$baktbpre='';
$phome_db_char='utf8';

//USER
$set_username='asback';
$set_password='a528ef895d163ff410b7c5c8f01ec234';
$set_loginauth='';
$set_loginrnd='n632LPQVFzfJSqsdApBZGY8xtHkwKB';
$set_outtime='60';
$set_loginkey='1';
$ebak_set_keytime=60;
$ebak_set_ckuseragent='';

//COOKIE
$phome_cookiedomain='';
$phome_cookiepath='/';
$phome_cookievarpre='ckpxvd_';

//LANGUAGE
$langr=ReturnUseEbakLang();
$ebaklang=$langr['lang'];
$ebaklangchar=$langr['langchar'];

//BAK
$bakpath='bdata';
$bakzippath='zip';
$filechmod='1';
$phpsafemod='';
$php_outtime='1000';
$limittype='';
$canlistdb='';
$ebak_set_moredbserver='';
$ebak_set_hidedbs='';
$ebak_set_escapetype='1';

//EBMA
$ebak_ebma_open=1;
$ebak_ebma_path='phpmyadmin';
$ebak_ebma_cklogin=0;

//SYS
$ebak_set_ckrndvar='pmvxnymbfrbr';
$ebak_set_ckrndval='ea7bb7bfcbbe2e665dd2333ba460f342';
$ebak_set_ckrndvaltwo='24d7136de402cff525d47b7acab3981b';
$ebak_set_ckrndvalthree='a399ca23e2c0a24dbcb106ab6a8c81c0';
$ebak_set_ckrndvalfour='4a03aa8946e2923c14e5ad993c3f8125';

//------------ SYSTEM ------------
HeaderIeChar();
?>