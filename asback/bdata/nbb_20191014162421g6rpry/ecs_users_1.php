<?php
define('InEmpireBakData',TRUE);
require("../../inc/header.php");

/*
		SoftName : EmpireBak Version 5.1
		Author   : wm_chief
		Copyright: Powered by www.phome.net
*/

DoSetDbChar('utf8');
E_D("DROP TABLE IF EXISTS `ecs_users`;");
E_C("CREATE TABLE `ecs_users` (
  `user_id` mediumint(8) unsigned NOT NULL auto_increment,
  `aite_id` text NOT NULL,
  `email` varchar(60) NOT NULL default '',
  `user_name` varchar(60) NOT NULL default '',
  `password` varchar(32) NOT NULL default '',
  `question` varchar(255) NOT NULL default '',
  `answer` varchar(255) NOT NULL default '',
  `sex` tinyint(1) unsigned NOT NULL default '0',
  `birthday` date NOT NULL default '0000-00-00',
  `user_money` decimal(10,2) NOT NULL default '0.00',
  `frozen_money` decimal(10,2) NOT NULL default '0.00',
  `pay_points` int(10) unsigned NOT NULL default '0',
  `rank_points` int(10) unsigned NOT NULL default '0',
  `address_id` mediumint(8) unsigned NOT NULL default '0',
  `reg_time` int(10) unsigned NOT NULL default '0',
  `last_login` int(11) unsigned NOT NULL default '0',
  `last_time` datetime NOT NULL default '0000-00-00 00:00:00',
  `last_ip` varchar(15) NOT NULL default '',
  `visit_count` smallint(5) unsigned NOT NULL default '0',
  `user_rank` tinyint(3) unsigned NOT NULL default '0',
  `is_special` tinyint(3) unsigned NOT NULL default '0',
  `ec_salt` varchar(10) default NULL,
  `salt` varchar(10) NOT NULL default '0',
  `parent_id` mediumint(9) NOT NULL default '0',
  `flag` tinyint(3) unsigned NOT NULL default '0',
  `alias` varchar(60) NOT NULL,
  `msn` varchar(60) NOT NULL,
  `qq` varchar(20) NOT NULL,
  `office_phone` varchar(20) NOT NULL,
  `home_phone` varchar(20) NOT NULL,
  `mobile_phone` varchar(20) NOT NULL,
  `is_validated` tinyint(3) unsigned NOT NULL default '0',
  `credit_line` decimal(10,2) unsigned NOT NULL,
  `passwd_question` varchar(50) default NULL,
  `passwd_answer` varchar(255) default NULL,
  `wxid` char(28) NOT NULL,
  `wxch_bd` char(2) NOT NULL,
  `nicheng` varchar(255) default NULL,
  `password_xkfla` varchar(40) NOT NULL,
  PRIMARY KEY  (`user_id`),
  UNIQUE KEY `user_name` (`user_name`),
  KEY `email` (`email`),
  KEY `parent_id` (`parent_id`),
  KEY `flag` (`flag`)
) ENGINE=MyISAM AUTO_INCREMENT=175 DEFAULT CHARSET=utf8");
E_D("replace into `ecs_users` values('174','',0x3939303239343839304071712e636f6d,0x627378627378,0x6262346335373633356535303132373234656662663861333836383637323634,'','','0','0000-00-00','0.00','0.00','0','0','0','1524463696','1524463696','0000-00-00 00:00:00',0x3232332e37332e3131352e313733,'1','0','0',NULL,'0','0','0','','','','','',0x3137363032303134333338,'0','0.00',NULL,NULL,'','',NULL,'');");
E_D("replace into `ecs_users` values('173','',0x37363139373338394071712e636f6d,0xe5af82e5af9ee79a84e68891,0x3664306463663465326532633739613532666265333434313862363239393438,'','','0','0000-00-00','0.00','0.00','0','0','62','1521327608','1524759866','0000-00-00 00:00:00',0x3131322e32322e3233352e3530,'34','0','0',0x39343935,'0','0','0','','','','','',0x3138383038383332363736,'0','0.00',NULL,NULL,'','',NULL,'');");
E_D("replace into `ecs_users` values('172','',0x646662617a68754071712e636f6d,0xe4ba8ce59388e79a84e4b896e7958c,0x6266323338613363383865663434666665386362663765373030313834356431,'','','0','0000-00-00','0.00','0.00','3','3','60','1517526852','1570913234','0000-00-00 00:00:00',0x3132372e302e302e31,'210','0','0',0x33393639,'0','0','0','','','','','',0x3135303130303335303439,'1','0.00',NULL,NULL,'','',NULL,'');");

require("../../inc/footer.php");
?>