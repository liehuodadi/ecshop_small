<?php
define('InEmpireBakData',TRUE);
require("../../inc/header.php");

/*
		SoftName : EmpireBak Version 5.1
		Author   : wm_chief
		Copyright: Powered by www.phome.net
*/

DoSetDbChar('utf8');
E_D("DROP TABLE IF EXISTS `ecs_back_order`;");
E_C("CREATE TABLE `ecs_back_order` (
  `back_id` mediumint(8) unsigned NOT NULL auto_increment,
  `delivery_sn` varchar(20) NOT NULL,
  `order_sn` varchar(20) NOT NULL,
  `order_id` mediumint(8) unsigned NOT NULL default '0',
  `invoice_no` varchar(50) default NULL,
  `add_time` int(10) unsigned default '0',
  `shipping_id` tinyint(3) unsigned default '0',
  `shipping_name` varchar(120) default NULL,
  `user_id` mediumint(8) unsigned default '0',
  `action_user` varchar(30) default NULL,
  `consignee` varchar(60) default NULL,
  `address` varchar(250) default NULL,
  `country` smallint(5) unsigned default '0',
  `province` smallint(5) unsigned default '0',
  `city` smallint(5) unsigned default '0',
  `district` smallint(5) unsigned default '0',
  `sign_building` varchar(120) default NULL,
  `email` varchar(60) default NULL,
  `zipcode` varchar(60) default NULL,
  `tel` varchar(60) default NULL,
  `mobile` varchar(60) default NULL,
  `best_time` varchar(120) default NULL,
  `postscript` varchar(255) default NULL,
  `how_oos` varchar(120) default NULL,
  `insure_fee` decimal(10,2) unsigned default '0.00',
  `shipping_fee` decimal(10,2) unsigned default '0.00',
  `update_time` int(10) unsigned default '0',
  `suppliers_id` smallint(5) default '0',
  `status` tinyint(1) unsigned NOT NULL default '0',
  `return_time` int(10) unsigned default '0',
  `agency_id` smallint(5) unsigned default '0',
  `refund_type` tinyint(1) NOT NULL default '0',
  `refund_desc` varchar(255) NOT NULL,
  `refund_money_1` decimal(10,2) NOT NULL default '0.00',
  `refund_money_2` decimal(10,2) NOT NULL default '0.00',
  `back_reason` varchar(255) NOT NULL default '',
  `goods_id` int(10) unsigned NOT NULL,
  `goods_name` varchar(255) NOT NULL default '',
  `status_back` tinyint(1) NOT NULL default '0' COMMENT '0:审核通过,1:收到寄回商品,2:换回商品已寄出,3:完成退货/返修,4:退款(无需退货),5:审核中,6:申请被拒绝,7:管理员取消,8:用户自己取消',
  `status_refund` tinyint(1) NOT NULL default '0' COMMENT '0:未退款,1:已退款',
  `imgs` text NOT NULL,
  `back_pay` tinyint(1) NOT NULL default '0',
  `back_type` varchar(1) NOT NULL default '0',
  `supplier_id` int(10) unsigned NOT NULL default '0',
  PRIMARY KEY  (`back_id`),
  KEY `user_id` (`user_id`),
  KEY `order_id` (`order_id`),
  KEY `goods_id` (`goods_id`)
) ENGINE=MyISAM AUTO_INCREMENT=5 DEFAULT CHARSET=utf8");
E_D("replace into `ecs_back_order` values('1','',0x32303136303830333933323835,'125',NULL,'1470165525','0',NULL,'168',NULL,0x6173646661736466,0x73616466617364666173,'1','2','52','500',NULL,NULL,'',NULL,'',NULL,'',NULL,'0.00','15.00','0','0','0','0','0','0','','134.40','0.00','','9',0xe788b1e9a39fe6b4bee58685e89299e58fa4e591bce4bca6e8b49de5b094e586b7e586bbe7949fe9b29ce7899be885b1e5ad90e882893130303067,'5','0','','2',0x31,'0');");
E_D("replace into `ecs_back_order` values('2','',0x32303136303830333933323835,'125',NULL,'1470165766','0',NULL,'168',NULL,0x6173646661736466,0x73616466617364666173,'1','2','52','500',NULL,NULL,'',NULL,'',NULL,'',NULL,'0.00','15.00','0','0','0','0','0','0','','134.40','0.00','','9',0xe788b1e9a39fe6b4bee58685e89299e58fa4e591bce4bca6e8b49de5b094e586b7e586bbe7949fe9b29ce7899be885b1e5ad90e882893130303067,'5','0','','2',0x31,'0');");
E_D("replace into `ecs_back_order` values('3','',0x32303136303830333131393132,'127',NULL,'1470169904','0',NULL,'168',NULL,0x6173646661736466,0x73616466617364666173,'1','2','52','500',NULL,NULL,'',NULL,'',NULL,0x646464,NULL,'0.00','15.00','0','0','0','0','0','0','','0.01','0.00','','49',0x31e58886e992b1e694afe4bb98e6b58be8af95e59586e59381,'5','0',0x2f696d616765732f75706c6f61642f696d6167652f32303136303830332f32303136303830333132333133375f34383937332e706e67,'2',0x34,'0');");
E_D("replace into `ecs_back_order` values('4','',0x32303136303830333131393132,'127',NULL,'1470170559','0',NULL,'168',NULL,0x6173646661736466,0x73616466617364666173,'1','2','52','500',NULL,NULL,'',NULL,'',NULL,'',NULL,'0.00','15.00','0','0','0','0','0','0','','7.20','0.00','','30',0xe6b3b0e59bbde88fa0e8909de89c9c31362d3138e696a42031e4b8aae8a385,'5','0',0x2f696d616765732f75706c6f61642f696d6167652f32303136303830332f32303136303830333132343233365f31383735332e706e67,'2',0x34,'0');");

require("../../inc/footer.php");
?>