<?php
define('InEmpireBakData',TRUE);
require("../../inc/header.php");

/*
		SoftName : EmpireBak Version 5.1
		Author   : wm_chief
		Copyright: Powered by www.phome.net
*/

DoSetDbChar('utf8');
E_D("DROP TABLE IF EXISTS `ecs_brand`;");
E_C("CREATE TABLE `ecs_brand` (
  `brand_id` smallint(5) unsigned NOT NULL auto_increment,
  `brand_name` varchar(60) NOT NULL default '',
  `brand_logo` varchar(80) NOT NULL default '',
  `brand_desc` text NOT NULL,
  `site_url` varchar(255) NOT NULL default '',
  `sort_order` tinyint(3) unsigned NOT NULL default '50',
  `is_show` tinyint(1) unsigned NOT NULL default '1',
  `brand_banner` varchar(80) NOT NULL COMMENT '商品品牌banner',
  PRIMARY KEY  (`brand_id`),
  KEY `is_show` (`is_show`)
) ENGINE=MyISAM AUTO_INCREMENT=14 DEFAULT CHARSET=utf8");
E_D("replace into `ecs_brand` values('11',0xe78388e781abe5a4a7e59cb0,0x313531383636303530373930373031383838362e706e67,0xe4b880e4b8aae5be88e69c89e6848fe6809de79a84e59381e7898ce590a7efbc8ce78388e781abe5a4a7e59cb0e7b3bbe58897e4b8bbe8a681e69c89e59084e7b1bb636d73e7b3bbe7bb9fe6a8a1e69dbfefbc8ce5bd93e784b6e4b99fe8bf98e58c85e590abe4b880e7b3bbe58897e69599e7a88befbc8ce59586e59f8ee7b1bbe6a8a1e69dbfe4b8bbe8a681e69c89656373686f70e887aae98082e5ba94e6a8a1e69dbfefbc8ce58685e5aeb9e7aea1e79086e7b1bbe4b8bbe8a681e69c8964656465636d73e887aae98082e5ba94e6a8a1e69dbfe58f8a576f72645072657373e887aae98082e5ba94e6a8a1e69dbfefbc8ce58fa6e5a496e8bf98e69c89e4b880e4ba9be5b08fe8bdafe4bbb6e58f8ae8bdafe4bbb6e4bdbfe794a8e69599e7a88befbc8ce5898de7abafe5bc80e58f91e69599e7a88be7ad89e38082,0x687474703a2f2f68747470733a2f2f7777772e646662617a68752e636f6d2f67726f75702d3134302d312e68746d6c,'50','1','');");
E_D("replace into `ecs_brand` values('12',0xe788b1e698afe4bda0e68891e8bdafe4bbb6,0x313531383635393331363330353136333335362e6a7067,0xe788b1e698afe4bda0e68891e8bdafe4bbb6e4b893e4b89ae4bb8ee4ba8be5beaee4bfa1e890a5e99480e7b3bbe58897e8bdafe4bbb6e79a84e7a094e58f91e4b88ee99480e594aeefbc8ce5ae98e696b9e5b7b2e68ea8e587bae5a49ae6acbee5beaee4bfa1e890a5e99480e8bdafe4bbb6efbc8ce788b1e698afe4bda0e68891e8bdafe4bbb6e5b7b2e694afe68c81e59084e7a78de7b1bbe59e8be79a84e6898be69cbae4bdbfe794a8efbc8ce58c85e68bace88bb9e69e9c694f53e59084e78988e69cace7b3bbe7bb9fefbc8c416e64726f6964e59084e78988e69cace7b3bbe7bb9fe58f8ae794b5e88491e78988e7ad89efbc8ce79baee5898de5b7b2e68890e4b8bae69c80e5a4a7e79a84e5beaee4bfa1e890a5e99480e8bdafe4bbb6e68f90e4be9be59586e4b98be4b880efbc8ce69797e4b88be4bba3e79086e59586e9818de5b883e585a8e59bbde59084e59cb0efbc8ce5a49ae6acbee8bdafe4bbb6e4bbbbe682a8e68c91e98089e38082,0x687474703a2f2f7777772e61736e65656c2e636f6d,'50','1','');");
E_D("replace into `ecs_brand` values('13',0x4e4242,'','','','50','1','');");

require("../../inc/footer.php");
?>