<?php
define('InEmpireBakData',TRUE);
require("../../inc/header.php");

/*
		SoftName : EmpireBak Version 5.1
		Author   : wm_chief
		Copyright: Powered by www.phome.net
*/

DoSetDbChar('utf8');
E_D("DROP TABLE IF EXISTS `ecs_back_goods`;");
E_C("CREATE TABLE `ecs_back_goods` (
  `rec_id` mediumint(8) unsigned NOT NULL auto_increment,
  `back_id` mediumint(8) unsigned default '0',
  `goods_id` mediumint(8) unsigned NOT NULL default '0',
  `product_id` mediumint(8) unsigned NOT NULL default '0',
  `product_sn` varchar(60) default NULL,
  `goods_name` varchar(120) default NULL,
  `brand_name` varchar(60) default NULL,
  `goods_sn` varchar(60) default NULL,
  `is_real` tinyint(1) unsigned default '0',
  `send_number` smallint(5) unsigned default '0',
  `goods_attr` text,
  `back_type` tinyint(1) unsigned NOT NULL default '0',
  `parent_id` int(10) unsigned NOT NULL,
  `back_goods_price` decimal(10,2) unsigned NOT NULL default '0.00',
  `back_goods_number` smallint(5) unsigned NOT NULL,
  `status_back` tinyint(1) NOT NULL default '0',
  `status_refund` tinyint(1) NOT NULL default '0',
  PRIMARY KEY  (`rec_id`),
  KEY `back_id` (`back_id`),
  KEY `goods_id` (`goods_id`)
) ENGINE=MyISAM AUTO_INCREMENT=5 DEFAULT CHARSET=utf8");
E_D("replace into `ecs_back_goods` values('1','1','9','0',NULL,0xe788b1e9a39fe6b4bee58685e89299e58fa4e591bce4bca6e8b49de5b094e586b7e586bbe7949fe9b29ce7899be885b1e5ad90e882893130303067,NULL,0x454353303030303039,'0','0','','0','0','134.40','1','5','0');");
E_D("replace into `ecs_back_goods` values('2','2','9','0',NULL,0xe788b1e9a39fe6b4bee58685e89299e58fa4e591bce4bca6e8b49de5b094e586b7e586bbe7949fe9b29ce7899be885b1e5ad90e882893130303067,NULL,0x454353303030303039,'0','0','','0','0','134.40','1','5','0');");
E_D("replace into `ecs_back_goods` values('3','3','49','0',NULL,0x31e58886e992b1e694afe4bb98e6b58be8af95e59586e59381,NULL,0x454353303030303439,'0','0','','4','0','0.01','1','5','0');");
E_D("replace into `ecs_back_goods` values('4','4','30','0',NULL,0xe6b3b0e59bbde88fa0e8909de89c9c31362d3138e696a42031e4b8aae8a385,NULL,0x454353303030303330,'0','0','','4','0','7.20','1','5','0');");

require("../../inc/footer.php");
?>