<?php
define('InEmpireBakData',TRUE);
require("../../inc/header.php");

/*
		SoftName : EmpireBak Version 5.1
		Author   : wm_chief
		Copyright: Powered by www.phome.net
*/

DoSetDbChar('utf8');
E_D("DROP TABLE IF EXISTS `ecs_account_log`;");
E_C("CREATE TABLE `ecs_account_log` (
  `log_id` mediumint(8) unsigned NOT NULL auto_increment,
  `user_id` mediumint(8) unsigned NOT NULL,
  `user_money` decimal(10,2) NOT NULL,
  `frozen_money` decimal(10,2) NOT NULL,
  `rank_points` mediumint(9) NOT NULL,
  `pay_points` mediumint(9) NOT NULL,
  `change_time` int(10) unsigned NOT NULL,
  `change_desc` varchar(255) NOT NULL,
  `change_type` tinyint(3) unsigned NOT NULL,
  PRIMARY KEY  (`log_id`),
  KEY `user_id` (`user_id`)
) ENGINE=MyISAM AUTO_INCREMENT=52 DEFAULT CHARSET=utf8");
E_D("replace into `ecs_account_log` values('8','5','99999999.99','0.00','0','0','1448230508',0x313131,'2');");
E_D("replace into `ecs_account_log` values('9','5','-0.01','0.00','0','0','1448231164',0xe694afe4bb98e8aea2e58d952032303135313132333931303936,'99');");
E_D("replace into `ecs_account_log` values('10','5','0.00','0.00','0','0','1448232942',0xe8aea2e58d95203230313531313233353631323220e8b5a0e98081e79a84e7a7afe58886,'99');");
E_D("replace into `ecs_account_log` values('11','5','-0.01','0.00','0','0','1448234136',0xe694afe4bb98e8aea2e58d952032303135313132333138383737,'99');");
E_D("replace into `ecs_account_log` values('12','5','0.00','0.00','0','0','1448242320',0xe8aea2e58d95203230313531313233363938353620e8b5a0e98081e79a84e7a7afe58886,'99');");
E_D("replace into `ecs_account_log` values('13','5','0.00','0.00','0','0','1448247519',0xe8aea2e58d95203230313531313233313436353720e8b5a0e98081e79a84e7a7afe58886,'99');");
E_D("replace into `ecs_account_log` values('14','5','0.00','0.00','0','0','1448250063',0xe8aea2e58d95203230313531313233363635333220e8b5a0e98081e79a84e7a7afe58886,'99');");
E_D("replace into `ecs_account_log` values('15','5','-111111.00','0.00','0','0','1448408235',0x313131,'2');");
E_D("replace into `ecs_account_log` values('16','5','-1111111.00','0.00','0','0','1448408273',0x313131,'2');");
E_D("replace into `ecs_account_log` values('17','5','-99999999.99','0.00','0','0','1448408290',0x31313131,'2');");
E_D("replace into `ecs_account_log` values('18','5','11111111.00','0.00','0','0','1448408322',0x31313131,'2');");
E_D("replace into `ecs_account_log` values('19','5','99999999.99','0.00','0','0','1448408338',0x313131,'2');");
E_D("replace into `ecs_account_log` values('20','5','-99999999.00','0.00','0','0','1448408358',0x313131,'2');");
E_D("replace into `ecs_account_log` values('21','5','899.01','0.00','0','0','1448408391',0x313131,'2');");
E_D("replace into `ecs_account_log` values('22','5','0.00','0.00','0','0','1448409431',0xe8aea2e58d95203230313531313233343437313220e8b5a0e98081e79a84e7a7afe58886,'99');");
E_D("replace into `ecs_account_log` values('23','5','0.00','0.00','0','0','1448409980',0xe8aea2e58d95203230313531313235363132353720e8b5a0e98081e79a84e7a7afe58886,'99');");
E_D("replace into `ecs_account_log` values('51','172','0.00','0.00','1','1','1522984384',0xe8aea2e58d95203230313830343036303338373520e8b5a0e98081e79a84e7a7afe58886,'99');");
E_D("replace into `ecs_account_log` values('50','172','0.00','0.00','1','1','1519009282',0xe8aea2e58d95203230313830323139373231323320e8b5a0e98081e79a84e7a7afe58886,'99');");
E_D("replace into `ecs_account_log` values('49','172','0.00','0.00','1','1','1519005402',0xe8aea2e58d95203230313830323139353134323520e8b5a0e98081e79a84e7a7afe58886,'99');");

require("../../inc/footer.php");
?>