<?php
define('InEmpireBakData',TRUE);
require("../../inc/header.php");

/*
		SoftName : EmpireBak Version 5.1
		Author   : wm_chief
		Copyright: Powered by www.phome.net
*/

DoSetDbChar('utf8');
E_D("DROP TABLE IF EXISTS `ecs_cart_combo`;");
E_C("CREATE TABLE `ecs_cart_combo` (
  `rec_id` mediumint(8) unsigned NOT NULL auto_increment,
  `user_id` mediumint(8) unsigned NOT NULL default '0',
  `session_id` char(32) character set utf8 collate utf8_bin NOT NULL default '',
  `goods_id` mediumint(8) unsigned NOT NULL default '0',
  `goods_sn` varchar(60) NOT NULL default '',
  `product_id` mediumint(8) unsigned NOT NULL default '0',
  `group_id` varchar(255) NOT NULL,
  `goods_name` varchar(120) NOT NULL default '',
  `market_price` decimal(10,2) unsigned NOT NULL default '0.00',
  `goods_price` decimal(10,2) NOT NULL default '0.00',
  `goods_number` smallint(5) unsigned NOT NULL default '0',
  `goods_attr` text NOT NULL,
  `is_real` tinyint(1) unsigned NOT NULL default '0',
  `extension_code` varchar(30) NOT NULL default '',
  `parent_id` mediumint(8) unsigned NOT NULL default '0',
  `rec_type` tinyint(1) unsigned NOT NULL default '0',
  `is_gift` smallint(5) unsigned NOT NULL default '0',
  `is_shipping` tinyint(1) unsigned NOT NULL default '0',
  `can_handsel` tinyint(3) unsigned NOT NULL default '0',
  `goods_attr_id` varchar(255) NOT NULL default '',
  `fencheng` varchar(255) default NULL,
  PRIMARY KEY  (`rec_id`),
  KEY `session_id` (`session_id`)
) ENGINE=MyISAM AUTO_INCREMENT=38 DEFAULT CHARSET=utf8");
E_D("replace into `ecs_cart_combo` values('20','0',0x3461303036353061333533646466666638316238396461616264613639376462,'49',0x454353303030303439,'0',0x6d5f676f6f64735f305f3439,0x31e58886e992b1e694afe4bb98e6b58be8af95e59586e59381,'0.01','0.01','1','','1','','0','0','0','1','0','',NULL);");
E_D("replace into `ecs_cart_combo` values('29','0',0x6333653262393963643762656138323735383166653431333062393736633435,'49',0x454353303030303439,'0',0x6d5f676f6f64735f305f3439,0x31e58886e992b1e694afe4bb98e6b58be8af95e59586e59381,'0.01','0.01','1','','1','','0','0','0','1','0','','0');");
E_D("replace into `ecs_cart_combo` values('30','0',0x6333653262393963643762656138323735383166653431333062393736633435,'1',0x454353303030303030,'4',0x6d5f676f6f64735f305f3439,0xe696b0e9b29ce6b0b4e69e9ce7949ce89c9ce9a699e88486e58d95e69e9ce7baa6383030e5858b,'231.60','193.00','1',0xe9878de9878f3a353030e5858b200ae5a496e8a7823ae7baa2e889b2200ae6acbee5bc8f3ae697b6e5b09ae6acbe200a,'1','','49','0','0','1','0',0x342c372c31,NULL);");
E_D("replace into `ecs_cart_combo` values('32','0',0x3436613131353637613863656464356562613633333135643839663136393637,'49',0x454353303030303439,'0',0x6d5f676f6f64735f305f3439,0x31e58886e992b1e694afe4bb98e6b58be8af95e59586e59381,'0.01','0.01','1','','1','','0','0','0','1','0','','0');");
E_D("replace into `ecs_cart_combo` values('33','0',0x3436613131353637613863656464356562613633333135643839663136393637,'1',0x454353303030303030,'4',0x6d5f676f6f64735f305f3439,0xe696b0e9b29ce6b0b4e69e9ce7949ce89c9ce9a699e88486e58d95e69e9ce7baa6383030e5858b,'231.60','193.00','1',0xe9878de9878f3a353030e5858b200ae5a496e8a7823ae7baa2e889b2200ae6acbee5bc8f3ae697b6e5b09ae6acbe200a,'1','','49','0','0','1','0',0x342c372c31,NULL);");
E_D("replace into `ecs_cart_combo` values('34','0',0x3436613131353637613863656464356562613633333135643839663136393637,'2',0x454353303030303032,'0',0x6d5f676f6f64735f305f3439,0xe794b0e784b6e7899be88289e5a4a7e9bb84e7939ce69da1e7949fe9b29ce7899be88289e586b7e586bbe79c9fe7a9bae9bb84e7899b,'105.60','88.00','1','','1','','49','0','0','1','0','',NULL);");
E_D("replace into `ecs_cart_combo` values('35','0',0x3436613131353637613863656464356562613633333135643839663136393637,'1',0x454353303030303030,'4',0x6d5f676f6f64735f305f31,0xe696b0e9b29ce6b0b4e69e9ce7949ce89c9ce9a699e88486e58d95e69e9ce7baa6383030e5858b,'231.60','156.00','1',0xe9878de9878f3a353030e5858b200ae5a496e8a7823ae7baa2e889b2200ae6acbee5bc8f3ae697b6e5b09ae6acbe200a,'1','','0','0','0','1','0',0x342c372c31,'0');");
E_D("replace into `ecs_cart_combo` values('36','0',0x3436613131353637613863656464356562613633333135643839663136393637,'11',0x454353303030303131,'0',0x6d5f676f6f64735f305f31,0xe6beb3e6b4b2e8bf9be58fa3313230e5a4a9e8b0b7e9a5b2e7899be4bb94e9aaa834e4bbbde58e9fe591b3e7949fe9b29c,'31.20','26.00','1','','1','','1','0','0','0','0','',NULL);");
E_D("replace into `ecs_cart_combo` values('37','0',0x3436613131353637613863656464356562613633333135643839663136393637,'10',0x454353303030303130,'0',0x6d5f676f6f64735f305f31,0xe58685e89299e696b0e9b29ce7899be8828931e696a4e6b885e79c9fe7949fe9b29ce7899be88289e781abe99485e9a39fe69d90,'105.60','88.00','1','','1','','1','0','0','0','0','',NULL);");

require("../../inc/footer.php");
?>