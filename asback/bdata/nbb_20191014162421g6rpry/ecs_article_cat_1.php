<?php
define('InEmpireBakData',TRUE);
require("../../inc/header.php");

/*
		SoftName : EmpireBak Version 5.1
		Author   : wm_chief
		Copyright: Powered by www.phome.net
*/

DoSetDbChar('utf8');
E_D("DROP TABLE IF EXISTS `ecs_article_cat`;");
E_C("CREATE TABLE `ecs_article_cat` (
  `cat_id` smallint(5) NOT NULL auto_increment,
  `cat_name` varchar(255) NOT NULL default '',
  `cat_type` tinyint(1) unsigned NOT NULL default '1',
  `keywords` varchar(255) NOT NULL default '',
  `cat_desc` varchar(255) NOT NULL default '',
  `sort_order` tinyint(3) unsigned NOT NULL default '50',
  `show_in_nav` tinyint(1) unsigned NOT NULL default '0',
  `parent_id` smallint(5) unsigned NOT NULL default '0',
  `page_title` varchar(255) NOT NULL,
  `page_seoh1` varchar(255) NOT NULL,
  PRIMARY KEY  (`cat_id`),
  KEY `cat_type` (`cat_type`),
  KEY `sort_order` (`sort_order`),
  KEY `parent_id` (`parent_id`)
) ENGINE=MyISAM AUTO_INCREMENT=12 DEFAULT CHARSET=utf8");
E_D("replace into `ecs_article_cat` values('1',0xe7b3bbe7bb9fe58886e7b1bb,'2','',0xe7b3bbe7bb9fe4bf9de79599e58886e7b1bb,'50','0','0','','');");
E_D("replace into `ecs_article_cat` values('2',0xe7bd91e5ba97e4bfa1e681af,'3','',0xe7bd91e5ba97e4bfa1e681afe58886e7b1bb,'50','0','1','','');");
E_D("replace into `ecs_article_cat` values('3',0xe7bd91e5ba97e5b8aee58aa9e58886e7b1bb,'4','',0xe7bd91e5ba97e5b8aee58aa9e58886e7b1bb,'50','1','1','','');");
E_D("replace into `ecs_article_cat` values('4',0x4e4242e4bb8be7bb8d,'1',0x6e62622c6e6262e4bb8be7bb8d2c6e6262e4bdbfe794a8e696b9e6b395,0x6e6262e4bdbfe794a8e696b9e6b395e8a786e9a291e6bc94e7a4baefbc8c6e6262e585ace58fb8e4bb8be7bb8de38081e69c89e585b3e6a48de789a9e7bb84e68890e983a8e58886e7ad89e8afa6e7bb86e4bb8be7bb8defbc8ce58fa6e5a496e68891e4bbace8bf98e4bc9ae7bb8fe5b8b8e69bb4e696b0e794a8e688b7e79a84e4bdbfe794a8e58f8de9a688e79bb8e585b3e59bbee78987e4be9be682a8e58f82e88083efbc8ce585ace58fb8e79a84e696b0e997bbe58aa8e68081e4b99fe58fafe4bba5e59ca8e8bf99e9878ce689bee588b0e38082,'50','0','0',0x4e4242e4bb8be7bb8de4bfa1e681af5f6e6262e4bdbfe794a8e696b9e6b3952ce5a29ee5a4a7e58e9fe790862ce7bb84e68890e983a8e58886e58f8ae794a8e688b7e6a188e4be8be59bbee78987,0x6e6e62e4bb8be7bb8de58f8ae4bdbfe794a8e696b9e6b395);");
E_D("replace into `ecs_article_cat` values('5',0x4e4242e696b0e997bbe58aa8e68081,'1',0x6e6262e696b0e997bb2c6e6262e69c80e696b0e4baa7e59381e4bfa1e681af2c6e6262e8b584e8aeaf,0xe5a682e69e9ce682a8e683b3e8a681e88eb7e5be974e4242e69c80e696b0e79a84e696b0e997bbe8b584e8aeafe58f8ae5ae98e696b9e79bb8e585b3e4baa7e59381e4bfa1e681afe8afb7e585b3e6b3a8e8bf99e9878cefbc8ce68891e4bbace4bc9ae5ae9ee697b6e69bb4e696b0e8b584e8aeafe58685e5aeb9efbc8c6e6262e4baa7e59381e696b0e58aa8e68081efbc8ce8bf99e4b99fe69c89e58aa9e4ba8ee682a8e883bde58f8ae697b6e8b79fe8bf9be4ba86e8a7a3efbc8ce5908ce697b6e68891e4bbace4b99fe58fafe4bba5e4b8bae682a8e68f90e4be9be69bb4e5a5bde79a84e69c8de58aa1e38082,'50','0','0',0x6e6262e696b0e997bbe58aa8e680815fe5ae98e696b9e69c80e696b0e4baa7e59381e4bfa1e681afe58f8ae585ace58fb8e58aa8e68081e8b584e8aeaf,0x6e6262e5ae98e696b9e69c80e696b0e4baa7e59381e4bfa1e681afe58f8ae585ace58fb8e58aa8e68081e8b584e8aeaf);");
E_D("replace into `ecs_article_cat` values('6',0x4e4242e794a8e688b7e58f8de9a688,'1','',0x4e4242e794a8e688b7e79c9fe5ae9ee58f8de9a688e59bbee78987e58f8ae79bb8e585b3e7bb8fe9aa8ce4baa4e6b581,'50','0','0','',0x4e4242e794a8e688b7e79c9fe5ae9ee58f8de9a688e59bbee78987e58f8ae79bb8e585b3e7bb8fe9aa8ce4baa4e6b581);");
E_D("replace into `ecs_article_cat` values('7',0xe794b7e5a3abe585bbe7949fe4bf9de581a5e7bb8fe9aa8ce5a4a7e585a8,'1',0xe794b7e5a3abe585bbe7949fe4bf9de581a52ce794b7e680a7e4bf9de581a5e7bb8fe9aa8ce696b9e6b3952ce794b7e4babae68c89e691a9737061e59bbee78987e5a4a7e585a8,0xe794b7e5a3abe585bbe7949fe4bf9de581a55fe794b7e680a7e4bf9de581a5e7bb8fe9aa8ce696b9e6b3952ce794b7e4babae68c89e691a9737061e59bbee78987e5a4a7e585a8,'50','0','0',0xe794b7e5a3abe585bbe7949fe4bf9de581a55fe794b7e680a7e4bf9de581a5e7bb8fe9aa8ce696b9e6b3952ce794b7e4babae68c89e691a9737061e59bbee78987e5a4a7e585a8,0xe794b7e5a3abe585bbe7949fe4bf9de581a5e7bb8fe9aa8ce58f8ae696b9e6b3952ce68c89e691a9535041e59bbee78987e5a4a7e585a8);");
E_D("replace into `ecs_article_cat` values('8',0x4e4242e794a8e688b7e6a188e4be8b,'1','','','50','0','0','','');");
E_D("replace into `ecs_article_cat` values('9',0x4e4242e4bdbfe794a8e696b9e6b395,'1','','','50','0','0','','');");
E_D("replace into `ecs_article_cat` values('10',0x6e6262e4bdbfe794a8e58f8ae58f8de9a688,'1','','','50','0','0','','');");
E_D("replace into `ecs_article_cat` values('11',0xe794b7e680a7e581a5e5bab7e5b8b8e8af86,'1','','','50','0','0','','');");

require("../../inc/footer.php");
?>