<?php
define('InEmpireBakData',TRUE);
require("../../inc/header.php");

/*
		SoftName : EmpireBak Version 5.1
		Author   : wm_chief
		Copyright: Powered by www.phome.net
*/

DoSetDbChar('utf8');
E_D("DROP TABLE IF EXISTS `ecs_template`;");
E_C("CREATE TABLE `ecs_template` (
  `filename` varchar(30) NOT NULL default '',
  `region` varchar(40) NOT NULL default '',
  `library` varchar(40) NOT NULL default '',
  `sort_order` tinyint(1) unsigned NOT NULL default '0',
  `id` smallint(5) unsigned NOT NULL default '0',
  `number` tinyint(1) unsigned NOT NULL default '5',
  `type` tinyint(1) unsigned NOT NULL default '0',
  `theme` varchar(60) NOT NULL default '',
  `remarks` varchar(30) NOT NULL default '',
  KEY `filename` (`filename`,`region`),
  KEY `theme` (`theme`),
  KEY `remarks` (`remarks`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8");
E_D("replace into `ecs_template` values(0x61727469636c65,'',0x2f6c6962726172792f7265636f6d6d656e645f626573742e6c6269,'0','0','3','0',0x6c696568756f5f6164617074696f6e,'');");
E_D("replace into `ecs_template` values(0x61727469636c65,'',0x2f6c6962726172792f7265636f6d6d656e645f686f742e6c6269,'0','0','3','0',0x6c696568756f5f6164617074696f6e,'');");
E_D("replace into `ecs_template` values(0x61727469636c65,'',0x2f6c6962726172792f7265636f6d6d656e645f70726f6d6f74696f6e2e6c6269,'0','0','3','0',0x6c696568756f5f6164617074696f6e,'');");

require("../../inc/footer.php");
?>