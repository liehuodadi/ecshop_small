<?php
define('InEmpireBakData',TRUE);
require("../../inc/header.php");

/*
		SoftName : EmpireBak Version 5.1
		Author   : wm_chief
		Copyright: Powered by www.phome.net
*/

DoSetDbChar('utf8');
E_D("DROP TABLE IF EXISTS `ecs_category`;");
E_C("CREATE TABLE `ecs_category` (
  `cat_id` smallint(5) unsigned NOT NULL auto_increment,
  `cat_name` varchar(90) NOT NULL default '',
  `keywords` varchar(255) NOT NULL default '',
  `cat_desc` varchar(255) NOT NULL default '',
  `parent_id` smallint(5) unsigned NOT NULL default '0',
  `sort_order` tinyint(1) unsigned NOT NULL default '50',
  `template_file` varchar(50) NOT NULL default '',
  `measure_unit` varchar(15) NOT NULL default '',
  `show_in_nav` tinyint(1) NOT NULL default '0',
  `style` varchar(150) NOT NULL,
  `is_show` tinyint(1) unsigned NOT NULL default '1',
  `grade` tinyint(4) NOT NULL default '0',
  `filter_attr` varchar(255) NOT NULL default '0',
  `is_top_style` int(3) unsigned NOT NULL default '0',
  `is_top_show` int(3) unsigned NOT NULL default '0',
  `cat_ico` varchar(255) NOT NULL,
  PRIMARY KEY  (`cat_id`),
  KEY `parent_id` (`parent_id`)
) ENGINE=MyISAM AUTO_INCREMENT=138 DEFAULT CHARSET=utf8");
E_D("replace into `ecs_category` values('130',0xe5beaee4bfa1e890a5e99480e8bdafe4bbb6,0xe5beaee4bfa1e890a5e99480e8bdafe4bbb6efbc8ce5beaee4bfa1e58aa0e4babae8bdafe4bbb6,0xe5beaee4bfa1e890a5e99480e8bdafe4bbb6e698afe794a8e4ba8ee8be85e58aa9e5beaee4bfa1e68ea8e5b9bfe79a84e5b7a5e585b7efbc8ce788b1e698afe4bda0e68891e7b3bbe58897e5beaee4bfa1e890a5e99480e8bdafe4bbb6e6bba1e8b6b3e682a8e79a84e68980e69c89e99c80e6b182efbc8ce4b88de8aebae698afe5beaee4bfa1e4bbbbe6848fe5ae9ae4bd8de58aa0e4babae8bf98e698afe8bdace58f91e38081e7bea4e58f91e7ad89e58a9fe883bdefbc8ce983bde698afe682a8e5819ae5beaee59586e5bf85e5a487e79a84e588a9e599a8efbc8ce5a4a7e9878fe79a84e6898be5b7a5e6938de4bd9ce5ae8ce585a8e58fafe4bba5e4bdbfe794a8e8bdafe4bbb6e58ebbe5ae8ce68890efbc8ce8aea9e5b7a5e4bd9ce69bb4e69c89e69588e78e87e38082,'0','50','','','1','','1','0','','0','0','');");
E_D("replace into `ecs_category` values('131',0xe88bb9e69e9c694f53e7b3bbe7bb9f,'','','130','50','','','0','','1','0','','0','0','');");
E_D("replace into `ecs_category` values('132',0xe5ae89e58d93416e64726f6964e7b3bbe7bb9f,'','','130','50','','','0','','1','0','','0','0','');");
E_D("replace into `ecs_category` values('133',0xe694afe4bb98e6b58be8af95,'','','0','50','','','0','','1','0','','0','0','');");
E_D("replace into `ecs_category` values('134',0xe7bd91e7ab99e7b3bbe7bb9f,'','','0','50','','','1','','1','0','','0','0','');");
E_D("replace into `ecs_category` values('135',0x656373686f70e6a8a1e69dbf,'','','134','50','','','0','','1','0','','0','0','');");
E_D("replace into `ecs_category` values('136',0x57696e646f7773e794b5e88491e78988,'','','130','50','','','0','','1','0','0','0','0','');");
E_D("replace into `ecs_category` values('137',0xe794b7e5a3abe68aa4e79086e794a8e59381,'','','0','50','','','0','','1','0','','0','0','');");

require("../../inc/footer.php");
?>