<?php
define('InEmpireBakData',TRUE);
require("../../inc/header.php");

/*
		SoftName : EmpireBak Version 5.1
		Author   : wm_chief
		Copyright: Powered by www.phome.net
*/

DoSetDbChar('utf8');
E_D("DROP TABLE IF EXISTS `ecs_sessions`;");
E_C("CREATE TABLE `ecs_sessions` (
  `sesskey` char(32) character set utf8 collate utf8_bin NOT NULL default '',
  `expiry` int(10) unsigned NOT NULL default '0',
  `userid` mediumint(8) unsigned NOT NULL default '0',
  `adminid` mediumint(8) unsigned NOT NULL default '0',
  `ip` char(15) NOT NULL default '',
  `user_name` varchar(60) NOT NULL,
  `user_rank` tinyint(3) NOT NULL,
  `discount` decimal(3,2) NOT NULL,
  `email` varchar(60) NOT NULL,
  `data` char(255) NOT NULL default '',
  PRIMARY KEY  (`sesskey`),
  KEY `expiry` (`expiry`)
) ENGINE=MEMORY DEFAULT CHARSET=utf8");
E_D("replace into `ecs_sessions` values(0x3364396434643964326632343963316232623536623465336431343466356430,'1571070381','0','0',0x3132372e302e302e31,'0','0','0.00','0',0x613a313a7b733a31303a226c6173745f636865636b223b693a313537313034313538313b7d);");
E_D("replace into `ecs_sessions` values(0x6166373334363534346162353830313136633962326662363637366666363965,'1571070063','0','0',0x3132372e302e302e31,'0','0','1.00','0',0x613a343a7b733a373a2266726f6d5f6164223b693a303b733a373a2272656665726572223b733a353a227063e7ab99223b733a31303a226c6f67696e5f6661696c223b693a303b733a31323a22636170746368615f776f7264223b733a31363a22597a457a5a6a56684f57557a59513d3d223b7d);");

require("../../inc/footer.php");
?>