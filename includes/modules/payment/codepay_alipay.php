<?php
/**
 * 码支付支付宝接口
 * ============================================================================
 * 版权所有 2005-2017 码支付保留所有权利。
 * 网站地址: http://codepay.fateqq.com
 * ----------------------------------------------------------------------------
 * 这是一个由码支付提供的支付宝 ecshop支付插件
 * 申请开通：https://codepay.fateqq.com/reg.html
 * 免签约 免手续费 及时到账
 * ============================================================================
 * $Author: codepay $
 */
//require(ROOT_PATH . 'includes/safety.php'); //3.6+版本可启用
if (!defined('IN_ECS'))
{
    die('Hacking attempt');
}

$payment_lang = ROOT_PATH . 'languages/zh_cn/payment/codepay_alipay.php';

if (file_exists($payment_lang))
{
    global $_LANG;

    include_once($payment_lang);
}

/* 模块的基本信息 */
if (isset($set_modules) && $set_modules == TRUE)
{
    $i = isset($modules) ? count($modules) : 0;

    /* 代码 */
    $modules[$i]['code']    = basename(__FILE__, '.php');

    /* 描述对应的语言项 */
    $modules[$i]['desc']    = 'codepay_alipay_desc';

    /* 是否支持货到付款 */
    $modules[$i]['is_cod']  = '0';

    /* 是否支持在线支付 */
    $modules[$i]['is_online']  = '1';

    /* 作者 */
    $modules[$i]['author']  = 'CODEPAY';

    /* 网址 */
    $modules[$i]['website'] = 'https://codepay.fateqq.com/i/17337';

    /* 版本号 */
    $modules[$i]['version'] = '1.0.0';

    /* 配置信息 */
    $modules[$i]['config']  = array(
        array('name' => 'codepay_id',           'type' => 'text',   'value' => ''),
        array('name' => 'codepay_key',               'type' => 'text',   'value' => ''),
        array('name' => 'codepay_act',           'type' => 'select',   'value' => ''),
        array('name' => 'codepay_return',               'type' => 'text',   'value' => $GLOBALS['ecs']->url() . 'respond.php'),

    );
    return;
}

/**
 * 类
 */
class codepay_alipay
{
    function __construct()
    {
        $this->codepay_alipay();
    }

    /**
     * 构造函数
     *
     * @access  public
     * @param
     *
     * @return void
     */
    function codepay_alipay()
    {
    }

    /**
     * 生成支付代码
     * @param   array   $order      订单信息
     * @param   array   $payment    支付方式信息
     */
    function get_code($order, $payment)
    {
        if (!defined('EC_CHARSET'))
        {
            $charset = 'utf-8';
        }
        else
        {
            $charset = EC_CHARSET;
        }

        //构造要请求的参数数组，无需改动
        $parameter = array(
            "id" => (int)$payment['codepay_id'],//平台ID号
            "type" => 1,//支付方式
            "price" => (float)$order['order_amount'],//原价
            "pay_id" => $order['log_id'], //可以是用户ID,站内商户订单号,用户名
            "param" => '',//自定义参数
            "act" => $payment['codepay_act'],//是否开启认证版的免挂机功能
            "outTime" => 360,//二维码超时设置
            "page" => 1,
            "debug" => 1,
            "notify_url"=>return_url(basename(__FILE__, '.php')),
            "return_url"=>return_url(basename(__FILE__, '.php')),
            "chart" => trim(strtolower($charset))//字符编码方式,
            //其他业务参数根据在线开发文档，添加参数.文档地址:https://codepay.fateqq.com/apiword/
            //如"参数名"=>"参数值"
        );

        ksort($parameter);
        reset($parameter);

        $param = '';
        $sign  = '';

        foreach ($parameter AS $key => $val)
        {
            if($val=='')continue;
            $param .= "$key=" .urlencode($val). "&";
            $sign  .= "$key=$val&";
        }

        $param = substr($param, 0, -1);
        $sign  = substr($sign, 0, -1). $payment['codepay_key'];
        $button = '<div style="text-align:center"><input type="button" onclick="window.open(\'https://api.xiuxiu888.com/creat_order/?'.$param. '&sign='.md5($sign).'\')" value="' .$GLOBALS['_LANG']['pay_button']. '" /></div>';
        return $button;
    }

    /**
     * 响应操作
     */
    function respond()
    {
        if (!empty($_POST))
        {
            foreach($_POST as $key => $data)
            {
                $_GET[$key] = $data;
            }
        }
        $payment  = get_payment('codepay_alipay');
        $order_sn = trim($_GET['pay_id']);
        /* 检查数字签名是否正确 */
        ksort($_GET);
        reset($_GET);
        $sign = '';
        foreach ($_GET AS $key=>$val)
        {
            if($val=='')continue;
            if ($key != 'sign' && $key != 'sign_type' && $key != 'code')
            {
                $sign .= "$key=$val&";
            }
        }

        $sign = substr($sign, 0, -1) . $payment['codepay_key'];

        if (md5($sign) != $_GET['sign'])
        {
            return false;
        }

        /* 检查支付的金额是否相符 */
        if (!check_money($order_sn, $_GET['price'])||!$_GET['pay_no']||!$_GET['money'])
        {
            return false;
        }


        order_paid($order_sn, 2);
        return true;
    }
}

?>
