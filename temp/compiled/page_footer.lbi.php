

<div class="container-fuild text-center ft-oth">
  <div>CopyRight &copy; 2017-<?php echo date("Y")?> <?php echo $this->_var['shop_name']; ?> 版权所有，并保留所有权利。</div>
  <div><?php echo $this->_var['shop_address']; ?> <?php echo $this->_var['shop_postcode']; ?> 
    <?php if ($this->_var['service_email']): ?> 
    E-mail: <?php echo $this->_var['service_email']; ?> 
    <?php endif; ?> 
  </div>
  <div class="container im"> 
    <?php $_from = $this->_var['qq']; if (!is_array($_from) && !is_object($_from)) { settype($_from, 'array'); }; $this->push_vars('', 'im');if (count($_from)):
    foreach ($_from AS $this->_var['im']):
?> 
    <?php if ($this->_var['im']): ?>
    <p><a href="https://wpa.qq.com/msgrd?v=3&uin=<?php echo $this->_var['im']; ?>&site=<?php echo $this->_var['shop_name']; ?>&menu=yes" target="_blank"><img src="//pub.idqqimg.com/wpa/images/counseling_style_52.png" border="0" alt="QQ咨询" title="QQ咨询" /></a> <br/>
      <i class="fa fa-qq" aria-hidden="true"></i> QQ号：<?php echo $this->_var['im']; ?><br/>
      <?php endif; ?> 
      
      <?php endforeach; endif; unset($_from); ?><?php $this->pop_vars();; ?> 
      <?php if ($this->_var['service_phone']): ?> 
      <i class="fa fa-phone" aria-hidden="true"></i> 咨询电话: <a href="tel:<?php echo $this->_var['service_phone']; ?>">158-1072-6210</a><br/>
      <i class="fa fa-weixin" aria-hidden="true"></i> 微信号：KingBaZhu</p>
    <?php endif; ?> 
  </div>
  <div> 
    <?php if ($this->_var['icp_number']): ?> 
    <?php echo $this->_var['lang']['icp_number']; ?>:<?php echo $this->_var['icp_number']; ?> 
    <?php endif; ?> 
    <?php if ($this->_var['stats_code']): ?> <span align="left"><?php echo $this->_var['stats_code']; ?></span> <?php endif; ?> </div>
</div>


<div class="container">
  <div class="df-right">
    <div class="df-right-meau meau-contact  hidden-xs"> <a href="javascript:" class="df-right-btn"> <span class="demo-icon"><i class="fa fa-comments" aria-hidden="true"></i></span>
      <p>在线客服</p>
      </a>
      <div class="df-right-box">
        <div class="box-border">
          <div class="sev-t"> <i class="fa fa-comments" aria-hidden="true"></i>
            <div><span onclick="window.open ('https://ssl.pop800.com/chat/212636','newwindow','height=549,width=500,top=50%,left=50%,toolbar=no,menubar=no,scrollbars=no, resizable=no,location=no, status=no')"><img src="themes/<?php echo $GLOBALS['_CFG']['template']; ?>/images/pop_online.gif" border="0" title="QQ咨询" style="cursor:pointer"/></span>
              <p>在线咨询，方便快捷</p>
              
              <?php $_from = $this->_var['qq']; if (!is_array($_from) && !is_object($_from)) { settype($_from, 'array'); }; $this->push_vars('', 'im');if (count($_from)):
    foreach ($_from AS $this->_var['im']):
?> 
              <?php if ($this->_var['im']): ?> 
              <a href="https://wpa.qq.com/msgrd?v=3&uin=<?php echo $this->_var['im']; ?>&site=<?php echo $this->_var['shop_name']; ?>&menu=yes" target="_blank"><img src="//pub.idqqimg.com/wpa/images/counseling_style_52.png" border="0" alt="QQ咨询" title="QQ咨询" /></a>
              <p>QQ号：<?php echo $this->_var['im']; ?></p>
              <?php endif; ?> 
              <?php endforeach; endif; unset($_from); ?><?php $this->pop_vars();; ?> 
            </div>
          </div>
          <div class="arrow-right"><i class="fa fa-stop"></i></div>
        </div>
      </div>
    </div>
    <div class="df-right-meau meau-contact"> <a href="javascript:" class="df-right-btn"> <span class="demo-icon"><i class="fa fa-volume-control-phone" aria-hidden="true"></i></span>
      <p>服务热线</p>
      </a>
      <div class="df-right-box">
        <div class="box-border">
          <div class="sev-t online"> <i class="fa fa-volume-control-phone" aria-hidden="true"></i> 
            <?php if ($this->_var['service_phone']): ?><a href="tel:<?php echo $this->_var['service_phone']; ?>">158-1072-6210</a><?php endif; ?>
            <p class="hidden-xs"><em>7*24小时客户服务热线</em></p>
            <p class="visible-xs text-center">（点击号码直接拨打）</p>
            <div class="clear"></div>
          </div>
          <div class="arrow-right"><i class="fa fa-stop"></i></div>
        </div>
      </div>
    </div>
    <div class="df-right-meau meau-code"> <a href="javascript:" class="df-right-btn"> <span class="demo-icon"><i class="fa fa-weixin"></i></span>
      <p>微信咨询</p>
      </a>
      <div class="df-right-box">
        <div class="box-border">
          <div class="sev-t">
            <div class="img"><img src="themes/<?php echo $GLOBALS['_CFG']['template']; ?>/images/weixin.jpg" class="img-responsive" /></div>
            <p>扫一扫加微信好友</p>
            <p>微信号：KingBaZhu</p>
            <p class="visible-xs">(长按复制微信号)</p>
          </div>
          <div class="arrow-right"><i class="fa fa-stop"></i></div>
        </div>
      </div>
    </div>
    <div class="df-right-meau meau-code"> <a href="goods-66.html" class="df-right-btn"> <span class="demo-icon"><i class="fa fa-shopping-cart"></i></span>
      <p>立即购买</p>
      </a> </div>
    <div class="lh-gotop text-center" id="lh-gotop"> <a href="javascript:;"><i class="fa fa-arrow-circle-up" aria-hidden="true"></i></a> </div>
    <script>
	$(function(){
		$(window).scroll(function(){
			if($(window).scrollTop()>100){  //距顶部多少像素时，出现返回顶部按钮
				$("#lh-gotop").fadeIn();	
			}
			else{
				$("#lh-gotop").hide();
			}
		});
		$("#lh-gotop").click(function(){
			$('html,body').animate({'scrollTop':0},500); //返回顶部动画 数值越小时间越短
		});
	});
</script> 
    <script type="text/javascript" src="themes/<?php echo $GLOBALS['_CFG']['template']; ?>/js/jquery-1.4.2.min.js"></script> 
  </div>
</div>
 

 
<script type="text/javascript" src="themes/<?php echo $GLOBALS['_CFG']['template']; ?>/js/liehuo_common.js"></script> 
<script type="text/javascript" src="themes/<?php echo $GLOBALS['_CFG']['template']; ?>/js/common.js"></script> 
<script type="text/javascript" src="themes/<?php echo $GLOBALS['_CFG']['template']; ?>/js/transport_jquery.js"></script>