<!doctype html>
<html lang="zh-cmn-Hans">
<head>
<meta name="Generator" content="烈火大地 自适应模板v1.0" />
<meta charset="utf-8">
<meta http-equiv="X-UA-Compatible" content="IE=edge">
<meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no">
<meta name="applicable-device" content="pc,mobile">
<meta name="Keywords" content="<?php echo $this->_var['keywords']; ?>" />
<meta name="Description" content="<?php echo $this->_var['description']; ?>" />
<title><?php echo $this->_var['article']['title']; ?>_<?php echo $this->_var['shop_name']; ?></title>
<link rel="shortcut icon" href="favicon.ico" />
<link rel="icon" href="animated_favicon.gif" type="image/gif" />
<link href="themes/<?php echo $GLOBALS['_CFG']['template']; ?>/css/bootstrap.css" rel="stylesheet" type="text/css">
<link href="themes/<?php echo $GLOBALS['_CFG']['template']; ?>/fontawesomemin.css" rel="stylesheet" type="text/css" />
<link href="themes/<?php echo $GLOBALS['_CFG']['template']; ?>/liehuomin.css" rel="stylesheet" type="text/css" />
<script type="text/javascript" src="themes/<?php echo $GLOBALS['_CFG']['template']; ?>/js/jquery-1.12.4.min.js"></script>
<script type="text/javascript" src="themes/<?php echo $GLOBALS['_CFG']['template']; ?>/js/bootstrap.js"></script>

<?php echo $this->smarty_insert_scripts(array('files'=>'common.js')); ?>
<script type="text/javascript" src="themes/liehuo_adaption/js/easydialog.min.js"></script>

<!--[if lt IE 9]>
      <script src="themes/<?php echo $GLOBALS['_CFG']['template']; ?>/js/html5shiv.min.js"></script>
      <script src="themes/<?php echo $GLOBALS['_CFG']['template']; ?>/js/respond.min.js"></script>
    <![endif]-->
</head>
<body>
<?php echo $this->fetch('library/page_header.lbi'); ?>
<div class="container bg-artnbb"> <?php echo $this->fetch('library/ur_here.lbi'); ?> 
  
  <div class="col-xs-12 col-lg-9">
    <div class="nbb-cont">
      <h1 class="text-center"><?php echo htmlspecialchars($this->_var['article']['title']); ?></h1>
      <p class="text-center"><?php echo htmlspecialchars($this->_var['article']['author']); ?> / <?php echo $this->_var['article']['add_time']; ?></p>
      <div class="nbbjj"> 
        <?php if ($this->_var['article']['content']): ?> 
        <?php echo $this->_var['article']['content']; ?> 
        <?php endif; ?> 
      </div>
      
       
      <?php if ($this->_var['article']['open_type'] == 2 || $this->_var['article']['open_type'] == 1): ?>
      <div class="col-xs-12"> <a href="<?php echo $this->_var['article']['file_url']; ?>" target="_blank" class="btn btn-success btn-sm">下载附件</a> </div>
      <?php endif; ?>
      <ul class="list-unstyled col-xs-12 nbb-sx">
        <?php if ($this->_var['prev_article']): ?>
        <li>上一篇：<a href="<?php echo $this->_var['prev_article']['url']; ?>"><?php echo $this->_var['prev_article']['title']; ?></a></li>
        <?php else: ?>
        <li>上一篇：没有了...</li>
        <?php endif; ?> 
        
        <?php if ($this->_var['next_article']): ?>
        <li>下一篇：<a href="<?php echo $this->_var['next_article']['url']; ?>"><?php echo $this->_var['next_article']['title']; ?></a></li>
        <?php else: ?>
        <li>下一篇：没有了...</li>
        <?php endif; ?>
      </ul>
    </div>
    <div class="lh-with-goods"> 
      <?php echo $this->fetch('library/goods_related.lbi'); ?> </div>
  </div>
   
  
  
  <div class="col-lg-3 visible-lg">
    <div class="lh-art-num"> <?php echo $this->fetch('library/article_related.lbi'); ?> </div>
    <div class="art-ad">
      <?php
			$GLOBALS['smarty']->assign('adlist',get_advlist('nbb介绍文章左侧',1));
		?>
      <?php $_from = $this->_var['adlist']; if (!is_array($_from) && !is_object($_from)) { settype($_from, 'array'); }; $this->push_vars('', 'ad');$this->_foreach['ad'] = array('total' => count($_from), 'iteration' => 0);
if ($this->_foreach['ad']['total'] > 0):
    foreach ($_from AS $this->_var['ad']):
        $this->_foreach['ad']['iteration']++;
?> 
      <?php echo $this->_var['ad']['content']; ?> 
      <?php endforeach; endif; unset($_from); ?><?php $this->pop_vars();; ?> 
    </div>
     
  </div>
   
  
</div>
 
<?php echo $this->fetch('library/nbbfoot_nav.lbi'); ?> 

 
<?php echo $this->fetch('library/page_footer.lbi'); ?> 
 
<?php echo $this->fetch('library/page_footer_com.lbi'); ?>
</body>
</html>
