
     <div class="gm-box row">
      <h4><?php echo $this->_var['lang']['bought_notes']; ?><small> [<?php echo $this->_var['lang']['later_bought_amounts']; ?><font class="f1"><?php echo $this->_var['pager']['record_count']; ?></font>]</small></h4>
	 </div>
      <div class="boxCenterList">
       <?php if ($this->_var['notes']): ?>
       <table class="table table-hover table-responsive">
       <tr>
		   <th>收货人</th>
		   <th>收货地址</th>
		   <th><?php echo $this->_var['lang']['number']; ?></th>
		   <th>支付方式</th>
		   <th><?php echo $this->_var['lang']['bought_time']; ?></th>
		</tr>
       <?php $_from = $this->_var['notes']; if (!is_array($_from) && !is_object($_from)) { settype($_from, 'array'); }; $this->push_vars('', 'note');if (count($_from)):
    foreach ($_from AS $this->_var['note']):
?>
       <tbody>
		   <tr>
		   <td><?php echo $this->_var['note']['consignee']; ?></td>
		   <td><?php echo $this->_var['note']['shengshixian']; ?></td>
		   <td><?php echo $this->_var['note']['goods_number']; ?></td>
		   <td><?php echo $this->_var['note']['pay_name']; ?></td>
		   <td><?php echo $this->_var['note']['add_time']; ?></td>
	</tr>
		   </tbody>
       <?php endforeach; endif; unset($_from); ?><?php $this->pop_vars();; ?>
       </table>
        <?php else: ?>
        <?php echo $this->_var['lang']['no_notes']; ?>
        <?php endif; ?>
       
       <div id="buy_pagebar" class="text-right">
        <form name="selectPageForm" action="<?php echo $_SERVER['PHP_SELF']; ?>" method="get">
        <?php if ($this->_var['pager']['styleid'] == 0): ?>
        <div id="buy_pager">
          共 <?php echo $this->_var['pager']['page_count']; ?> 页 <span> <a href="<?php echo $this->_var['pager']['page_first']; ?>"><?php echo $this->_var['lang']['page_first']; ?></a> <a href="<?php echo $this->_var['pager']['page_prev']; ?>"><?php echo $this->_var['lang']['page_prev']; ?></a> <a href="<?php echo $this->_var['pager']['page_next']; ?>"><?php echo $this->_var['lang']['page_next']; ?></a> <a href="<?php echo $this->_var['pager']['page_last']; ?>"><?php echo $this->_var['lang']['page_last']; ?></a> </span>
            <?php $_from = $this->_var['pager']['search']; if (!is_array($_from) && !is_object($_from)) { settype($_from, 'array'); }; $this->push_vars('key', 'item_0_95015100_1571994212');if (count($_from)):
    foreach ($_from AS $this->_var['key'] => $this->_var['item_0_95015100_1571994212']):
?>
            <input type="hidden" name="<?php echo $this->_var['key']; ?>" value="<?php echo $this->_var['item_0_95015100_1571994212']; ?>" />
            <?php endforeach; endif; unset($_from); ?><?php $this->pop_vars();; ?>
        </div>
        <?php else: ?>

        
         <div id="buy_pager" class="pagebar">
          <span class="f_l f6" style="margin-right:10px;"><?php echo $this->_var['lang']['total']; ?> <b><?php echo $this->_var['pager']['record_count']; ?></b> <?php echo $this->_var['lang']['user_comment_num']; ?></span>
          <?php if ($this->_var['pager']['page_first']): ?><a href="<?php echo $this->_var['pager']['page_first']; ?>">1 ...</a><?php endif; ?>
          <?php if ($this->_var['pager']['page_prev']): ?><a class="prev" href="<?php echo $this->_var['pager']['page_prev']; ?>"><?php echo $this->_var['lang']['page_prev']; ?></a><?php endif; ?>
          <?php $_from = $this->_var['pager']['page_number']; if (!is_array($_from) && !is_object($_from)) { settype($_from, 'array'); }; $this->push_vars('key', 'item_0_95091400_1571994212');if (count($_from)):
    foreach ($_from AS $this->_var['key'] => $this->_var['item_0_95091400_1571994212']):
?>
                <?php if ($this->_var['pager']['page'] == $this->_var['key']): ?>
                <span class="page_now"><?php echo $this->_var['key']; ?></span>
                <?php else: ?>
                <a href="<?php echo $this->_var['item_0_95091400_1571994212']; ?>">[<?php echo $this->_var['key']; ?>]</a>
                <?php endif; ?>
            <?php endforeach; endif; unset($_from); ?><?php $this->pop_vars();; ?>

          <?php if ($this->_var['pager']['page_next']): ?><a class="next" href="<?php echo $this->_var['pager']['page_next']; ?>"><?php echo $this->_var['lang']['page_next']; ?></a><?php endif; ?>
          <?php if ($this->_var['pager']['page_last']): ?><a class="last" href="<?php echo $this->_var['pager']['page_last']; ?>">...<?php echo $this->_var['pager']['page_count']; ?></a><?php endif; ?>
          <?php if ($this->_var['pager']['page_kbd']): ?>
            <?php $_from = $this->_var['pager']['search']; if (!is_array($_from) && !is_object($_from)) { settype($_from, 'array'); }; $this->push_vars('key', 'item_0_95182400_1571994212');if (count($_from)):
    foreach ($_from AS $this->_var['key'] => $this->_var['item_0_95182400_1571994212']):
?>
            <input type="hidden" name="<?php echo $this->_var['key']; ?>" value="<?php echo $this->_var['item_0_95182400_1571994212']; ?>" />
            <?php endforeach; endif; unset($_from); ?><?php $this->pop_vars();; ?>
            <kbd style="float:left; margin-left:8px; position:relative; bottom:3px;"><input type="text" name="page" onkeydown="if(event.keyCode==13)selectPage(this)" size="3" class="B_blue" /></kbd>
            <?php endif; ?>
        </div>
        

        <?php endif; ?>
        </form>
        <script type="Text/Javascript" language="JavaScript">
        <!--
        
        function selectPage(sel)
        {
          sel.form.submit();
        }
        
        //-->
        </script>
      </div>
      
     
      </div>
   
  