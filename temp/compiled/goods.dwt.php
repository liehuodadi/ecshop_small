<!doctype html>
<html lang="zh-cmn-Hans">
<head>
<meta name="Generator" content="烈火大地 自适应模板v1.0" />
<meta charset="utf-8">
<meta http-equiv="X-UA-Compatible" content="IE=edge">
<meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no">
<meta name="applicable-device" content="pc,mobile">
<meta name="Keywords" content="<?php echo $this->_var['keywords']; ?>" />
<meta name="Description" content="<?php echo $this->_var['description']; ?>" />
<title><?php echo $this->_var['page_title']; ?></title>
<link rel="shortcut icon" href="favicon.ico" />
<link rel="icon" href="animated_favicon.gif" type="image/gif" />
<link href="<?php echo $this->_var['ecs_css_path']; ?>" rel="stylesheet" type="text/css" />
<link href="themes/<?php echo $GLOBALS['_CFG']['template']; ?>/css/bootstrap.css" rel="stylesheet" type="text/css">
<link href="themes/<?php echo $GLOBALS['_CFG']['template']; ?>/fontawesomemin.css" rel="stylesheet" type="text/css" />
<link href="themes/<?php echo $GLOBALS['_CFG']['template']; ?>/liehuomin.css" rel="stylesheet" type="text/css" />
<link href="themes/<?php echo $GLOBALS['_CFG']['template']; ?>/css/viewer.css" rel="stylesheet" type="text/css">
<link href="themes/liehuo_adaption/quick_buy.css" rel="stylesheet" type="text/css" />
<script type="text/javascript" src="themes/<?php echo $GLOBALS['_CFG']['template']; ?>/js/jquery-1.12.4.min.js"></script>
<script type="text/javascript" src="themes/<?php echo $GLOBALS['_CFG']['template']; ?>/js/bootstrap.js"></script>

<script type="text/javascript" src="themes/liehuo_adaption/js/magiczoom_plus.js"></script>
<script type="text/javascript" src="themes/liehuo_adaption/js/common.js"></script>
<script language="javascript">
function shows_number(result)
{
     if(result.product_number !=undefined){
         document.getElementById('shows_number').innerHTML = result.product_number+'<?php if ($this->_var['goods']['measure_unit']): ?><?php echo $this->_var['goods']['measure_unit']; ?><?php else: ?>件<?php endif; ?>';
     }else{
         document.getElementById('shows_number').innerHTML = '无库存';
     }
}
//默认就显示第一个属性库存
function changeKucun()
{
var frm=document.forms['ECS_FORMBUY'];
spec_arr = getSelectedAttributes(frm);
    if(spec_arr==''){
         document.getElementById('shows_number').innerHTML = '<?php echo $this->_var['goods']['goods_number']; ?><?php if ($this->_var['goods']['measure_unit']): ?><?php echo $this->_var['goods']['measure_unit']; ?><?php else: ?>件<?php endif; ?>';
    }else{
         Ajax.call('goods.php?act=get_products_info', 'id=' + spec_arr+ '&goods_id=' + goods_id, shows_number, 'GET', 'JSON');
    }
}
</script>
<script type="text/javascript">
function $id(element) {
  return document.getElementById(element);
}
//切屏--是按钮，_v是内容平台，_h是内容库
function reg(str){
  var bt=$id(str+"_b").getElementsByTagName("h2");
  for(var i=0;i<bt.length;i++){
    bt[i].subj=str;
    bt[i].pai=i;
    bt[i].style.cursor="pointer";
    bt[i].onclick=function(){
      $id(this.subj+"_v").innerHTML=$id(this.subj+"_h").getElementsByTagName("blockquote")[this.pai].innerHTML;
      for(var j=0;j<$id(this.subj+"_b").getElementsByTagName("h2").length;j++){
        var _bt=$id(this.subj+"_b").getElementsByTagName("h2")[j];
        var ison=j==this.pai;
        _bt.className=(ison"":"h2bg");
      }
    }
  }
  $id(str+"_h").className="none";
  $id(str+"_v").innerHTML=$id(str+"_h").getElementsByTagName("blockquote")[0].innerHTML;
}
function countNum(i) {
	var $count_box = $("#skunum");
	var $input = $count_box.find('input');
	var num = $input.val();
    if (!$.isNumeric(num)) {
		alert("请您输入正确的购买数量!")
        $input.val('1');
        return;
	}
    num = parseInt(num) + i;
    switch (true) {
		case num == 0:
			$input.val('1');
			alert('您至少得购买1件商品！');
			break;
        default:
        	$input.val(num);
            break;
    }
	changePrice();
}
</script>
<script type="text/javascript" src="themes/liehuo_adaption/js/quick_buy1.js"></script>
<!--[if lt IE 9]>
	<script src="themes/<?php echo $GLOBALS['_CFG']['template']; ?>/js/html5shiv.min.js"></script>
    <script src="themes/<?php echo $GLOBALS['_CFG']['template']; ?>/js/respond.min.js"></script>
<![endif]-->
</head>
<body>
<?php echo $this->fetch('library/page_header.lbi'); ?> 
<script type="text/javascript" src="themes/liehuo_adaption/js/magiczoomplus.js"></script> 
<script type="text/javascript" src="themes/liehuo_adaption/js/easydialog.min.js"></script>
<div class="container goods-back"> 
	<div class="hidden-xs">当前位置：<a href="/" target="_blank">NBB</a> > <?php echo $this->_var['goods']['goods_style_name']; ?></div>
   <?php echo $this->fetch('library/goods_gallery_m.lbi'); ?> 
    
    <div class="col-xs-12 col-md-7 lh-goods-info">
      <form action="javascript:addToCart(<?php echo $this->_var['goods']['goods_id']; ?>)" method="post" name="ECS_FORMBUY" id="ECS_FORMBUY" >
        
         <div class="gd-flr">
            <h1><?php echo $this->_var['goods']['goods_style_name']; ?></h1>
            <?php if ($this->_var['goods']['goods_brief']): ?>
            <p class="des"><?php echo $this->_var['goods']['goods_brief']; ?></p>
            <?php endif; ?> 
		  </div>
                        
            <ul class="list-unstyled pro">
              <?php if ($this->_var['goods']['is_shipping']): ?>
              <!--<li class="ft9"> <?php echo $this->_var['lang']['goods_free_shipping']; ?>： </li>-->
              <?php endif; ?> 
              <?php if ($this->_var['cfg']['show_goodssn']): ?>
              <li> <span class="lbl"><?php echo $this->_var['lang']['goods_sn']; ?></span> <em><?php echo $this->_var['goods']['goods_sn']; ?></em> </li>
              
              <?php endif; ?> 
              
              <?php if ($this->_var['cfg']['show_addtime']): ?>
              <li> <span class="lbl"><?php echo $this->_var['lang']['add_time']; ?></span> <em><?php echo $this->_var['goods']['add_time']; ?></em> </li>
              
              <?php endif; ?> 
              <?php if ($this->_var['cfg']['show_goodsweight']): ?>
              <li> <span class="lbl"><?php echo $this->_var['lang']['goods_weight']; ?></span> <em><?php echo $this->_var['goods']['goods_weight']; ?></em> </li>
              
              <?php endif; ?> 
              
              <?php if ($this->_var['cfg']['show_marketprice']): ?>
              <li> <span class="lbl"><?php echo $this->_var['lang']['market_price']; ?>：</span> <del><?php echo $this->_var['goods']['market_price']; ?></del> </li>
              <?php endif; ?>
              <li> <a class="ljgoumai" href="javascript:void(0);" name="ljgoumai" rel="nofollow"></a> 
                <?php if ($this->_var['goods']['is_promote'] && $this->_var['goods']['gmt_end_time']): ?> 
                <script type="text/javascript" src="themes/liehuo_adaption/js/lefttime.js"></script> 
                <span class="qianggou">抢购</span> <span class="lbl"><?php echo $this->_var['lang']['promote_price']; ?></span> <span class="unit"> <strong id="ECS_SHOPPRICE"><?php echo $this->_var['goods']['promote_price']; ?></strong> </span> <span class="timedown" id="timedown"><i class="fa fa-clock-o" aria-hidden="true"></i> 剩余时间：<strong id="leftTime"><?php echo $this->_var['lang']['please_waiting']; ?></strong></span> 
                <?php else: ?> 
                <span class="lbl"><?php echo $this->_var['lang']['shop_price']; ?>：</span> <span class="unit"> <strong id="ECS_SHOPPRICE"><?php echo $this->_var['goods']['shop_price_formated']; ?></strong> </span> 
                
				  <?php if ($this->_var['goods']['is_shipping']): ?>
              <small class="baoyou">顺丰包邮</small>
              <?php endif; ?> 
				  
                <?php if ($this->_var['rank_prices']): ?>
                <table class="table table-striped">
                  <tbody>
                    <?php $_from = $this->_var['rank_prices']; if (!is_array($_from) && !is_object($_from)) { settype($_from, 'array'); }; $this->push_vars('key', 'rank_price');$this->_foreach['rank_price'] = array('total' => count($_from), 'iteration' => 0);
if ($this->_foreach['rank_price']['total'] > 0):
    foreach ($_from AS $this->_var['key'] => $this->_var['rank_price']):
        $this->_foreach['rank_price']['iteration']++;
?>
                    <tr id="ECS_RANKPRICE_<?php echo $this->_var['key']; ?>">
                      <td><?php echo $this->_var['rank_price']['rank_name']; ?></td>
                      <td><?php echo $this->_var['rank_price']['price']; ?></td>
                    </tr>
                    <?php endforeach; endif; unset($_from); ?><?php $this->pop_vars();; ?>
                  </tbody>
                </table>
                <?php endif; ?>
                <?php endif; ?> 
				   
              </li>
              
              <?php if ($this->_var['goods']['give_integral'] > 0): ?>
              <li><span><?php echo $this->_var['lang']['goods_give_integral']; ?>可获<em class="red"><?php echo $this->_var['goods']['give_integral']; ?></em> <?php echo $this->_var['points_name']; ?></span></li>
              <?php endif; ?> 
              
              <?php if ($this->_var['cfg']['use_integral'] > 0): ?>
              <li><span><?php echo $this->_var['lang']['goods_integral']; ?><em class="red"><?php echo $this->_var['goods']['integral']; ?></em> <?php echo $this->_var['points_name']; ?></span></li>
              <?php endif; ?> 
              <?php if ($this->_var['goods']['goods_brand'] != "" && $this->_var['cfg']['show_brand']): ?>
              <li> <span><?php echo $this->_var['lang']['goods_brand']; ?>：</span> <?php echo $this->_var['goods']['goods_brand']; ?></li>
              <?php endif; ?> 
              
              <?php if ($this->_var['promotion']): ?>
              <li class="gd-cuxiao"> 
                <?php $_from = $this->_var['promotion']; if (!is_array($_from) && !is_object($_from)) { settype($_from, 'array'); }; $this->push_vars('key', 'item');if (count($_from)):
    foreach ($_from AS $this->_var['key'] => $this->_var['item']):
?> 
                <span class="lbl"><?php echo $this->_var['lang']['activity']; ?></span> 
                <?php if ($this->_var['item']['type'] == "snatch"): ?> 
                <a href="snatch.php" title="<?php echo $this->_var['lang']['snatch']; ?>">[<?php echo $this->_var['lang']['snatch']; ?>]</a> 
                <?php elseif ($this->_var['item']['type'] == "group_buy"): ?> 
                <a href="group_buy.php" title="<?php echo $this->_var['lang']['group_buy']; ?>">[<?php echo $this->_var['lang']['group_buy']; ?>]</a> 
                <?php elseif ($this->_var['item']['type'] == "auction"): ?> 
                <a href="auction.php" title="<?php echo $this->_var['lang']['auction']; ?>">[<?php echo $this->_var['lang']['auction']; ?>]</a> 
                <?php elseif ($this->_var['item']['type'] == "favourable"): ?> 
                <a href="activity.php" title="<?php echo $this->_var['lang']['favourable']; ?>">[<?php echo $this->_var['lang']['favourable']; ?>]</a> 
                <?php endif; ?> 
                <a href="<?php echo $this->_var['item']['url']; ?>" title="<?php echo $this->_var['lang'][$this->_var['item']['type']]; ?> <?php echo $this->_var['item']['act_name']; ?><?php echo $this->_var['item']['time']; ?>"><?php echo $this->_var['item']['act_name']; ?></a><br />
                <?php endforeach; endif; unset($_from); ?><?php $this->pop_vars();; ?> 
              </li>
              <?php endif; ?>
              
              <li><span class="lbl">最近售出<em class="red"><?php echo $this->_var['sales_count']; ?></em> 件</span>（<a href="#pjxqitem" class="pingjia">已有 <?php echo $this->_var['goods']['comments_number']; ?> 人评价</a>）</li>
            </ul>
             
            
            <div class="lh-chek">
              <ul class="list-unstyled col-xs-12">
                
                 
                <?php $_from = $this->_var['specification']; if (!is_array($_from) && !is_object($_from)) { settype($_from, 'array'); }; $this->push_vars('spec_key', 'spec');if (count($_from)):
    foreach ($_from AS $this->_var['spec_key'] => $this->_var['spec']):
?>
                <li>
                  <!--<div class="lbl2 col-xs-12">选择<?php echo $this->_var['spec']['name']; ?>：</div>-->
                   
                  <?php if ($this->_var['spec']['attr_type'] == 1): ?> 
                  <?php if ($this->_var['cfg']['goodsattr_style'] == 1): ?>
                  <input type="hidden" name="spec_attr_type" value="<?php echo $this->_var['spec_key']; ?>">
                  <div class="ys_xuan col-xs-12 row" id="xuan_<?php echo $this->_var['spec_key']; ?>">
                    <div class="catt" id="catt_<?php echo $this->_var['spec_key']; ?>"> 
                      <?php $_from = $this->_var['spec']['values']; if (!is_array($_from) && !is_object($_from)) { settype($_from, 'array'); }; $this->push_vars('key', 'value');if (count($_from)):
    foreach ($_from AS $this->_var['key'] => $this->_var['value']):
?> 
                      <a <?php if ($this->_var['value']['disabled']): ?>class="wuxiao"<?php else: ?><?php if ($this->_var['value']['selected_key_mb5'] == '1'): ?>class="cattsel"<?php endif; ?><?php endif; ?> onclick="<?php if ($this->_var['value']['disabled']): ?><?php else: ?>show_attr_status(this,<?php echo $this->_var['goods']['goods_id']; ?><?php if ($this->_var['attr_id']): ?>,<?php echo $this->_var['attr_id']; ?><?php endif; ?>);<?php if ($this->_var['spec_key'] == $this->_var['attr_id']): ?>get_gallery_attr(<?php echo $this->_var['id']; ?>, <?php echo $this->_var['value']['id']; ?>);<?php endif; ?><?php endif; ?>" name="<?php echo $this->_var['value']['id']; ?>" id="xuan_a_<?php echo $this->_var['value']['id']; ?>">
                      <p <?php if ($this->_var['value']['thumb_url']): ?>class="padd" style="background:url() no-repeat transparent;"<?php elseif ($this->_var['value']['hex_color'] != ''): ?>style="background:#<?php echo $this->_var['value']['hex_color']; ?>; height:2rem; width:2rem"<?php else: ?>style="padding:0 0;"<?php endif; ?> title="<?php echo $this->_var['value']['label']; ?>">
                      <?php if ($this->_var['value']['thumb_url']): ?> <img class="lh-xuan" src="<?php echo $this->_var['value']['thumb_url']; ?>" width="40" height="40" alt="<?php echo $this->_var['value']['label']; ?>"> <?php elseif ($this->_var['value']['hex_color']): ?>
                      <?php else: ?> <em><?php echo $this->_var['value']['label']; ?></em> <?php endif; ?><span class="shuxp"><?php echo $this->_var['value']['label']; ?></span> <i></i>
                      </p>
                      <input style="display:none" id="spec_value_<?php echo $this->_var['value']['id']; ?>" type="radio" name="spec_<?php echo $this->_var['spec_key']; ?>" value="<?php echo $this->_var['value']['id']; ?>" <?php if ($this->_var['value']['selected_key_mb5'] == '1'): ?>checked<?php endif; ?> />
                      </a> 
                      <?php endforeach; endif; unset($_from); ?><?php $this->pop_vars();; ?> 
                    </div>
                  </div>
                  <input type="hidden" name="spec_list" value="<?php echo $this->_var['key']; ?>" />
                  
                  <?php else: ?>
                  <select name="spec_<?php echo $this->_var['spec_key']; ?>">
                    <?php $_from = $this->_var['spec']['values']; if (!is_array($_from) && !is_object($_from)) { settype($_from, 'array'); }; $this->push_vars('key', 'value');if (count($_from)):
    foreach ($_from AS $this->_var['key'] => $this->_var['value']):
?>
                    <option label="<?php echo $this->_var['value']['label']; ?>" value="<?php echo $this->_var['value']['id']; ?>"><?php echo $this->_var['value']['label']; ?> <?php if ($this->_var['value']['price'] > 0): ?><?php echo $this->_var['lang']['plus']; ?><?php elseif ($this->_var['value']['price'] < 0): ?><?php echo $this->_var['lang']['minus']; ?><?php endif; ?><?php if ($this->_var['value']['price'] != 0): ?><?php echo $this->_var['value']['format_price']; ?><?php endif; ?></option>
                    <?php endforeach; endif; unset($_from); ?><?php $this->pop_vars();; ?>
                  </select>
                  <input type="hidden" name="spec_list" value="<?php echo $this->_var['key']; ?>" />
                  <?php endif; ?> 
                  <?php else: ?> 
                  <?php $_from = $this->_var['spec']['values']; if (!is_array($_from) && !is_object($_from)) { settype($_from, 'array'); }; $this->push_vars('key', 'value');if (count($_from)):
    foreach ($_from AS $this->_var['key'] => $this->_var['value']):
?>
                  <label for="spec_value_<?php echo $this->_var['value']['id']; ?>">
                    <input type="checkbox" name="spec_<?php echo $this->_var['spec_key']; ?>" value="<?php echo $this->_var['value']['id']; ?>" id="spec_value_<?php echo $this->_var['value']['id']; ?>" onclick="changePrice()" />
                    <?php echo $this->_var['value']['label']; ?> [<?php if ($this->_var['value']['price'] > 0): ?><?php echo $this->_var['lang']['plus']; ?><?php elseif ($this->_var['value']['price'] < 0): ?><?php echo $this->_var['lang']['minus']; ?><?php endif; ?> <?php echo $this->_var['value']['format_price']; ?>] </label>
                  <br />
                  <?php endforeach; endif; unset($_from); ?><?php $this->pop_vars();; ?>
                  <input type="hidden" name="spec_list" value="<?php echo $this->_var['key']; ?>" />
                  <?php endif; ?> 
                </li>
                <?php endforeach; endif; unset($_from); ?><?php $this->pop_vars();; ?> 
                
                <script type="text/javascript">
				var myString=new Array();
				
				<?php $_from = $this->_var['prod_exist_arr']; if (!is_array($_from) && !is_object($_from)) { settype($_from, 'array'); }; $this->push_vars('pkey', 'prod');if (count($_from)):
    foreach ($_from AS $this->_var['pkey'] => $this->_var['prod']):
?>
				myString[<?php echo $this->_var['pkey']; ?>]="<?php echo $this->_var['prod']; ?>";
				<?php endforeach; endif; unset($_from); ?><?php $this->pop_vars();; ?>
				
			</script> 
                
                
                <li class="skunum_li col-xs-12"> <span class="lbl flt">购买数量：</span>
                  <div class="skunum" id="skunum"> <span class="minus" title="减少1个数量"><i class="iconfont">-</i></span>
                    <input id="number" name="number" type="text" min="1" value="1" onchange="countNum(0)">
                    <span class="add" title="增加1个数量"><i class="iconfont">+</i></span> <cite class="storage"> 件 </cite> </div>
                  <div class="skunum" id="skunum"> 
                    <?php if ($this->_var['goods']['goods_number'] != "" && $this->_var['cfg']['show_goodsnumber']): ?> 
                    <cite class="kucun">库存（<font id="shows_number">载入中··· </font>）</cite> 
                    <?php endif; ?> 
                  </div>
                </li>
                
                
                <li class="add_cart_li col-xs-12">
                  <div class="col-xs-12"><a href="javascript:addToCart(<?php echo $this->_var['goods']['goods_id']; ?>)" class="btn btn btn-danger btn-lg btn-block" role="button">立即购买</a></div>
                  </li>
					
              </ul>
				
				<div class="col-xs-12"><a class="btn btn-default btn-xs" role="button" href="javascript:collect(<?php echo $this->_var['goods']['goods_id']; ?>)"><i class="fa fa-heart" aria-hidden="true"></i>收藏</a> 
                    <?php if ($this->_var['affiliate']['on']): ?> 
                    <a class="btn btn-default btn-xs" href="user.php?act=affiliate&goodsid=<?php echo $this->_var['goods']['goods_id']; ?>"><i class="fa fa-hand-o-right" aria-hidden="true"></i>推荐给好友</a></div>
                  <?php endif; ?> 
                <div class="col-xs-12">
                  <div class="tips">
                    <p><strong>温馨提示：</strong>会员在线支付可获得积分！支持支付宝和QQ钱包在线支付。 </p>
                  </div>
                </div>
				
				
            </div>
		  
      </form>
    </div>
	  <?php echo $this->fetch('library/goods_gallery.lbi'); ?> 
    
	
  <div class="lh-cont col-xs-12">
    <div class="row">
      <div class="procont-left">
        <div class="product_tabs">
          <div class="sectionbox z-box" id="spxqitem">
            <div class="spxq_main">
              <p>产品名称：<?php echo $this->_var['goods']['goods_name']; ?></p>
              <div class="spxq_dec col-12"><?php echo $this->_var['goods']['goods_desc']; ?></div>
            </div>
          </div>
        </div>
      </div>
      
    </div>
  </div>
	
	<div class="col-xs-12 buyjilu">
		<?php echo $this->fetch('library/bought_note_guide.lbi'); ?>
	</div>
	<div class="z-box sectionbox"> <?php echo $this->fetch('library/comments.lbi'); ?> </div>
	
	</div>
	
	
	
 
<?php echo $this->fetch('library/nbbfoot_nav_goods.lbi'); ?>
<div class="add_ok" id="cart_show">
  <div class="tip hidden-sm-down"> <i class="fa fa-check" aria-hidden="true"></i>商品已成功加入购物车 </div>
  <div class="go"> <a href="javascript:easyDialog.close();" class="back">&lt;&lt;继续购物</a> <a href="flow.php" class="btn">去结算</a> </div>
</div>
<?php echo $this->fetch('library/page_footer.lbi'); ?> <?php echo $this->fetch('library/quick_buy.lbi'); ?> 
 
<?php echo $this->fetch('library/page_footer_goods.lbi'); ?> 
<script type="text/javascript" src="themes/liehuo_adaption/js/jquery-1.9.1.min.js"></script> 
<script type="text/javascript" src="themes/liehuo_adaption/js/liehuo_goods.js"></script> 
<script type="text/javascript" src="themes/liehuo_adaption/js/transport_jquery.js"></script> 
<script type="text/javascript" src="themes/liehuo_adaption/js/utils.js"></script> 
<script type="text/javascript" src="themes/liehuo_adaption/js/region.js"></script> 
<script type="text/javascript" src="themes/liehuo_adaption/js/jquery.SuperSlide.js"></script> 
<script type="text/javascript" src="themes/liehuo_adaption/js/jquery.json.js"></script> 
<script type="text/javascript" src="themes/liehuo_adaption/js/liehuo_common.js"></script> 
<script type="text/javascript" src="themes/liehuo_adaption/js/common.js"></script>
</body>
<script type="text/javascript">
var goods_id = <?php echo $this->_var['goods_id']; ?>;
var goodsattr_style = <?php echo empty($this->_var['cfg']['goodsattr_style']) ? '1' : $this->_var['cfg']['goodsattr_style']; ?>;
var gmt_end_time = <?php echo empty($this->_var['promote_end_time']) ? '0' : $this->_var['promote_end_time']; ?>;
<?php $_from = $this->_var['lang']['goods_js']; if (!is_array($_from) && !is_object($_from)) { settype($_from, 'array'); }; $this->push_vars('key', 'item');if (count($_from)):
    foreach ($_from AS $this->_var['key'] => $this->_var['item']):
?>
var <?php echo $this->_var['key']; ?> = "<?php echo $this->_var['item']; ?>";
<?php endforeach; endif; unset($_from); ?><?php $this->pop_vars();; ?>
var goodsId = <?php echo $this->_var['goods_id']; ?>;
var now_time = <?php echo $this->_var['now_time']; ?>;


onload = function(){
  changePrice();
  changeKucun();//这里是添加的
  fixpng();
  try {onload_leftTime();}
  catch (e) {}
}

/**
 * 点选可选属性或改变数量时修改商品价格的函数
 */
function changePrice()
{
  var attr = getSelectedAttributes(document.forms['ECS_FORMBUY']);

  var qty = document.forms['ECS_FORMBUY'].elements['number'].value;

  Ajax.call('goods.php', 'act=price&id=' + goodsId + '&attr=' + attr + '&number=' + qty, changePriceResponse, 'GET', 'JSON');
}

/**
 * 接收返回的信息
 */
function changePriceResponse(res)
{
  if (res.err_msg.length > 0)
  {
    alert(res.err_msg);
  }
  else
  {
    
    if (document.getElementById('ECS_SHOPPRICE'))
      document.getElementById('ECS_SHOPPRICE').innerHTML = res.result;
	 if (document.getElementById('ECS_SHOPPRICE_TOP'))
      document.getElementById('ECS_SHOPPRICE_TOP').innerHTML = res.result;
    if (document.getElementById('ECS_GOODS_AMOUNT'))
      document.getElementById('ECS_GOODS_AMOUNT').innerHTML = res.result;
	
  }
}

</script>
</html>
