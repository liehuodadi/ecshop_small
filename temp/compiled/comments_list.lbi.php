<div class="z-detail-point-box col-xs-12">
  <div class="row">
    <div class="z-detail-point-box-left col-md-6">
      <p><font>温馨提示：</font>为了您能尽快收到NBB修护膏，请尽量先<a href="/user.php?act=register" target="_blank">注册会员</a>，通过本站在线支付的方式下单，所有当天下午5点之前支付成功的订单，当天发货！</p>
    </div>
    <div class="z-detail-point-box-right col-md-6">
      <p>欢迎对使用过NBB商品进行评价，<font>评论隐私保护：</font>1.匿名评论；2.注册用户评论(加密处理)
        
        <?php if ($this->_var['factor'] == 0): ?>用户可放心发表使用后的心得感受 <a href="javascript:void(0);"  id="go_com" rel="nofollow" data-toggle="modal" data-target="#myModal"> 我要评价 </a> <?php elseif ($this->_var['factor'] == 1): ?>只有登陆的用户才可以对该商品 <a href="javascript:void(0);"  id="go_com" rel="nofollow" data-toggle="modal" data-target="#myModal"> 我要评价 </a> <?php elseif ($this->_var['factor'] == 2): ?>在本站购买过一次其他商品的用户才可以对该商品 <a href="javascript:void(0);"  id="go_com" rel="nofollow" data-toggle="modal" data-target="#myModal"> 我要评价 </a> <?php elseif ($this->_var['factor'] == 3): ?>购买过此<?php echo $this->_var['goods_name']; ?>的顾客，在收到商品才可以对该商品 <a href="javascript:void(0);"  id="go_com" rel="nofollow" data-toggle="modal" data-target="#myModal"> 我要评价 </a> <?php endif; ?> 
      </p>
    </div>
  </div>
</div>
<div class="z-select col-xs-12"> <a class="pjxqitem" href="javascript:void(0);" name="pjxqitem" rel="nofollow">全部评价</a>（<?php echo $this->_var['pager']['record_count']; ?>） </div>
<div id="ECS_COMMENT">
  <div class="z-com-list col-xs-12"> 
    <?php if ($this->_var['comments']): ?> 
    <?php $_from = $this->_var['comments']; if (!is_array($_from) && !is_object($_from)) { settype($_from, 'array'); }; $this->push_vars('', 'comment');if (count($_from)):
    foreach ($_from AS $this->_var['comment']):
?>
    <ul class="list-unstyled z-com col-xs-12">
      <li> 
        <?php if ($this->_var['comment']['username']): ?><span class="u-name"><?php echo htmlspecialchars($this->_var['comment']['username']); ?></span><?php else: ?><span class="u-name"><?php echo $this->_var['lang']['anonymous']; ?></span><?php endif; ?><span class="min_star text-right"><cite class="ping_star"><i style="width:<?php echo $this->_var['comment']['rank_w']; ?>%;"></i></cite></span> </li>
      <li class="z-coms" id="comment_id_1329143">
        <div class="z-coms-text"> <i class="fa fa-quote-left" aria-hidden="true"></i><?php echo $this->_var['comment']['content']; ?><span class="text-right"> <i class="fa fa-quote-right" aria-hidden="true"></i></span> </div>
        <?php if ($this->_var['comment']['re_content']): ?>
        <div class="z-com-hf col-xs-12"> 
          <i class="fa fa-commenting-o" aria-hidden="true"></i><em><?php echo $this->_var['lang']['admin_username']; ?></em><?php echo $this->_var['comment']['re_content']; ?> </div>
        <?php endif; ?> 
      </li>
    </ul>
    
    <?php endforeach; endif; unset($_from); ?><?php $this->pop_vars();; ?> 
    <?php else: ?>
    <div class="col-xs-12 no-pinglun"><?php echo $this->_var['lang']['no_comments']; ?></div>
    <?php endif; ?>
	      <ul class="list-unstyled col-xs-12">
	  <li>
	<h4>更多微信、QQ等用户实拍反馈图及使用经验请看：[<a href="/article_cat-6.html" target="_blank">用户反馈</a>]</h4>
		  </li>
	  </ul>
    
    <div class="pagenav col-xs-12"> <span><a class="btn btn-success" href="javascript:void(0);"  rel="nofollow" id="go_com" data-toggle="modal" data-target="#myModal">我要评论</a></span> <span class="text-right">
      <form name="selectPageForm" action="<?php echo $_SERVER['PHP_SELF']; ?>" method="get">
		  共 <?php echo $this->_var['pager']['page_count']; ?> 页评论
        <?php if ($this->_var['pager']['page_prev'] != "javascript:;"): ?> 
        <a href="<?php echo $this->_var['pager']['page_prev']; ?>" class="step"><?php echo $this->_var['lang']['page_prev']; ?></a> 
        <?php else: ?> 
        <a href="<?php echo $this->_var['pager']['page_prev']; ?>" class="step" style="color:#ccc;"><?php echo $this->_var['lang']['page_prev']; ?></a> 
        <?php endif; ?> 
        <?php $_from = $this->_var['pager']['page_number']; if (!is_array($_from) && !is_object($_from)) { settype($_from, 'array'); }; $this->push_vars('key', 'item_0_12850900_1571994213');if (count($_from)):
    foreach ($_from AS $this->_var['key'] => $this->_var['item_0_12850900_1571994213']):
?> 
        <?php if ($this->_var['pager']['page'] == $this->_var['key']): ?> 
        <span class="currentStep"><?php echo $this->_var['key']; ?></span> 
        <?php else: ?> 
        <a href="<?php echo $this->_var['item_0_12850900_1571994213']; ?>" class="step"><?php echo $this->_var['key']; ?></a> 
        <?php endif; ?> 
        <?php endforeach; endif; unset($_from); ?><?php $this->pop_vars();; ?> 
        
        <?php if ($this->_var['pager']['page_next'] != "javascript:;"): ?><a href="<?php echo $this->_var['pager']['page_next']; ?>" class="step"><?php echo $this->_var['lang']['page_next']; ?></a><?php else: ?><a href="<?php echo $this->_var['pager']['page_next']; ?>" class="step" style="color:#ccc;"><?php echo $this->_var['lang']['page_next']; ?></a><?php endif; ?> 
        
        <?php $_from = $this->_var['pager']['search']; if (!is_array($_from) && !is_object($_from)) { settype($_from, 'array'); }; $this->push_vars('key', 'item_0_12995400_1571994213');if (count($_from)):
    foreach ($_from AS $this->_var['key'] => $this->_var['item_0_12995400_1571994213']):
?>
        <input type="hidden" name="<?php echo $this->_var['key']; ?>" value="<?php echo $this->_var['item_0_12995400_1571994213']; ?>" />
        <?php endforeach; endif; unset($_from); ?><?php $this->pop_vars();; ?>
      </form>
      </span> </div>
  </div>
</div>
<script type="Text/Javascript" language="JavaScript">
        <!--
        
        function selectPage(sel)
        {
          sel.form.submit();
        }
        
        //-->
        </script> 



<div class="modal fade" id="myModal" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
  <div class="modal-dialog modal-lg" role="document">
    <div class="modal-content">
      <div class="modal-body col-xs-12">
        <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
        
       
		
       
        <form action="javascript:;" onsubmit="submitComment(this)" method="post" name="commentForm">
          
          <ul class="list-unstyled col-xs-12" id="addr-form">
            <li class="form-inline">
              <label for="exampleInputName2"><?php echo $this->_var['lang']['username']; ?></label>
              <?php if ($_SESSION['user_name']): ?><?php echo $_SESSION['user_name']; ?><?php else: ?><?php echo $this->_var['lang']['anonymous']; ?><?php endif; ?> 
            </li>
            <li class="form-inline">
              <div class="form-group">
                <div class="input-group">
                  <div class="input-group-addon">E-mail</div>
                  <input type="text" class="form-control" name="email" id="email" value="<?php echo htmlspecialchars($_SESSION['email']); ?>">
                </div>
              </div>
            </li>
            <li class="checkbox"> <span><?php echo $this->_var['lang']['comment_rank']; ?></span>
              <label>
                <input name="comment_rank" type="radio" value="1" id="comment_rank1" />
                <img src="themes/liehuo_adaption/images/stars1.gif" /> </label>
              <label>
                <input name="comment_rank" type="radio" value="2" id="comment_rank2" />
                <img src="themes/liehuo_adaption/images/stars2.gif" /> </label>
              <label>
                <input name="comment_rank" type="radio" value="3" id="comment_rank3" />
                <img src="themes/liehuo_adaption/images/stars3.gif" /> </label>
              <label>
                <input name="comment_rank" type="radio" value="4" id="comment_rank4" />
                <img src="themes/liehuo_adaption/images/stars4.gif" /> </label>
              <label>
                <input name="comment_rank" type="radio" value="5" checked="checked" id="comment_rank5" />
                <img src="themes/liehuo_adaption/images/stars5.gif" /> </label>
            </li>
          
            <li class="form-group">
              <label><?php echo $this->_var['lang']['comment_content']; ?></label>
              <textarea name="content" class="form-control"></textarea>
            </li>
            <?php if ($this->_var['enabled_captcha']): ?>
            <li class="form-inline">
              <div class="form-group lh-yzma">
                <div class="input-group">
                  <div class="input-group-addon"><?php echo $this->_var['lang']['comment_captcha']; ?></div>
                  <input type="text" class="form-control" id="exampleInputAmount" name="captcha" placeholder="请填写验证码">
                  <div class="input-group-addon"><img src="captcha.php?<?php echo $this->_var['rand']; ?>" alt="captcha" onClick="this.src='captcha.php?'+Math.random()" class="yanz"/></div>
                </div>
              </div>
            </li>
            <?php endif; ?>
            
            <li class="lh-pinglun">
              <input type="hidden" name="cmt_type" value="<?php echo $this->_var['comment_type']; ?>" />
              <input type="hidden" name="id" value="<?php echo $this->_var['id']; ?>" />
              <label>&nbsp;&nbsp;&nbsp;&nbsp;</label>
              <input name="" type="submit"  value="提交评论" class="btn btn-success">
              <button type="button" class="btn btn-default" data-dismiss="modal">关闭窗口</button>
            </li>
          </ul>
        </form>
      </div>
      <div class="modal-footer"> </div>
    </div>
  </div>
</div>

 

<script type="text/javascript">
//<![CDATA[
<?php $_from = $this->_var['lang']['cmt_lang']; if (!is_array($_from) && !is_object($_from)) { settype($_from, 'array'); }; $this->push_vars('key', 'item_0_13130300_1571994213');if (count($_from)):
    foreach ($_from AS $this->_var['key'] => $this->_var['item_0_13130300_1571994213']):
?>
var <?php echo $this->_var['key']; ?> = "<?php echo $this->_var['item_0_13130300_1571994213']; ?>";
<?php endforeach; endif; unset($_from); ?><?php $this->pop_vars();; ?>

/**
 * 提交评论信息
*/
function submitComment(frm)
{
  var cmt = new Object;

  //cmt.username        = frm.elements['username'].value;
  cmt.email           = frm.elements['email'].value;
  cmt.content         = frm.elements['content'].value;
  cmt.type            = frm.elements['cmt_type'].value;
  cmt.id              = frm.elements['id'].value;
  cmt.enabled_captcha = frm.elements['enabled_captcha'] ? frm.elements['enabled_captcha'].value : '0';
  cmt.captcha         = frm.elements['captcha'] ? frm.elements['captcha'].value : '';
  cmt.rank            = 0;

  for (i = 0; i < frm.elements['comment_rank'].length; i++)
  {
    if (frm.elements['comment_rank'][i].checked)
    {
       cmt.rank = frm.elements['comment_rank'][i].value;
     }
  }

//  if (cmt.username.length == 0)
//  {
//     alert(cmt_empty_username);
//     return false;
//  }

  if (cmt.email.length > 0)
  {
     if (!(Utils.isEmail(cmt.email)))
     {
        alert(cmt_error_email);
        return false;
      }
   }
   else
   {
        alert(cmt_empty_email);
        return false;
   }

   if (cmt.content.length == 0)
   {
      alert(cmt_empty_content);
      return false;
   }

   if (cmt.enabled_captcha > 0 && cmt.captcha.length == 0 )
   {
      alert(captcha_not_null);
      return false;
   }

   Ajax.call('comment.php', 'cmt=' + $.toJSON(cmt), commentResponse, 'POST', 'JSON');
   return false;
}

/**
 * 处理提交评论的反馈信息
*/
  function commentResponse(result)
  {
    if (result.message)
    {
      alert(result.message);
    }

    if (result.error == 0)
    {
      var layer = document.getElementById('ECS_COMMENT');

      if (layer)
      {
        layer.innerHTML = result.content;
      }
    }
	easyDialog.close();
  	window.location.reload();
  }
  
	function commentsFrom(){
		easyDialog.open({
			  container : 'commentsFrom'
		});	
	}

//]]>

</script>