<?php if ($this->_var['related_goods']): ?>

<h2>相关商品</h2>
<ul class="list-inline">
  
  <?php $_from = $this->_var['related_goods']; if (!is_array($_from) && !is_object($_from)) { settype($_from, 'array'); }; $this->push_vars('', 'releated_goods_data');if (count($_from)):
    foreach ($_from AS $this->_var['releated_goods_data']):
?>
  <li class="col-xs-12 col-sm-4"> <a class="col-xs-5 col-sm-12" href="<?php echo $this->_var['releated_goods_data']['url']; ?>" target="_blank"> <img class="img-responsive" alt="<?php echo $this->_var['releated_goods_data']['goods_name']; ?>" src="<?php echo $this->_var['releated_goods_data']['goods_thumb']; ?>"></a>
    <div class="col-xs-7 col-sm-12 gdr">
      <p> 
        <?php if ($this->_var['releated_goods_data']['promote_price'] != 0): ?><?php echo $this->_var['lang']['promote_price']; ?>：<?php echo $this->_var['releated_goods_data']['formated_promote_price']; ?> <?php else: ?> 
        <?php echo $this->_var['lang']['shop_price']; ?>：<?php echo $this->_var['releated_goods_data']['shop_price']; ?> 
        <?php endif; ?><br/>
        市场价：<del><?php echo $this->_var['releated_goods_data']['market_price']; ?></del></p>
      <h4><a href="<?php echo $this->_var['releated_goods_data']['url']; ?>" target="_blank"><?php echo $this->_var['releated_goods_data']['short_name']; ?></a></h4>
    </div>
  </li>
  
  <?php endforeach; endif; unset($_from); ?><?php $this->pop_vars();; ?>
</ul>

<?php endif; ?>