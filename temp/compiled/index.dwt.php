<!doctype html>
<html lang="zh-cmn-Hans">
<head>
<meta name="Generator" content="烈火大地 自适应模板v1.0" />
<meta charset="utf-8">
<meta http-equiv="X-UA-Compatible" content="IE=edge">
<meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no">
<meta name="applicable-device" content="pc,mobile">
<meta name="Keywords" content="<?php echo $this->_var['keywords']; ?>" />
<meta name="Description" content="<?php echo $this->_var['description']; ?>" />
<title><?php echo $this->_var['page_title']; ?></title>
<link rel="shortcut icon" href="favicon.ico" />
<link rel="icon" href="animated_favicon.gif" type="image/gif" />
<link href="themes/<?php echo $GLOBALS['_CFG']['template']; ?>/css/bootstrap.css" rel="stylesheet" type="text/css">
<link href="themes/<?php echo $GLOBALS['_CFG']['template']; ?>/fontawesomemin.css" rel="stylesheet" type="text/css" />
<link href="themes/<?php echo $GLOBALS['_CFG']['template']; ?>/liehuomin.css" rel="stylesheet" type="text/css" />
<script type="text/javascript" src="themes/<?php echo $GLOBALS['_CFG']['template']; ?>/js/jquery-1.12.4.min.js"></script>
<script type="text/javascript" src="themes/<?php echo $GLOBALS['_CFG']['template']; ?>/js/bootstrap.js"></script>

<!--[if lt IE 9]>
      <script src="themes/<?php echo $GLOBALS['_CFG']['template']; ?>/js/html5shiv.min.js"></script>
      <script src="themes/<?php echo $GLOBALS['_CFG']['template']; ?>/js/respond.min.js"></script>
    <![endif]-->
</head>
<body>
<?php echo $this->fetch('library/page_header.lbi'); ?> 

 
<?php echo $this->fetch('library/nbb.lbi'); ?> 
 
<?php echo $this->fetch('library/nbbfoot_nav.lbi'); ?> 


<div class="container sy-zixun">
  <ul class="col-xs-12 col-sm-6 list-unstyled">
    <?php $_from = $this->_var['class_articles_5']; if (!is_array($_from) && !is_object($_from)) { settype($_from, 'array'); }; $this->push_vars('', 'article');if (count($_from)):
    foreach ($_from AS $this->_var['article']):
?>
    <li><a href="<?php echo $this->_var['article']['url']; ?>" title="<?php echo htmlspecialchars($this->_var['article']['title']); ?>" target="_blank"><?php echo sub_str($this->_var['article']['short_title'],15); ?></a></li>
    <?php endforeach; endif; unset($_from); ?><?php $this->pop_vars();; ?>
  </ul>
  <ul class="col-xs-12 col-sm-6 list-unstyled">
    <?php $_from = $this->_var['class_articles_6']; if (!is_array($_from) && !is_object($_from)) { settype($_from, 'array'); }; $this->push_vars('', 'article');if (count($_from)):
    foreach ($_from AS $this->_var['article']):
?>
    <li><a href="<?php echo $this->_var['article']['url']; ?>" title="<?php echo htmlspecialchars($this->_var['article']['title']); ?>" target="_blank"><?php echo sub_str($this->_var['article']['short_title'],15); ?></a></li>
    <?php endforeach; endif; unset($_from); ?><?php $this->pop_vars();; ?>
  </ul>
</div>

 
<?php echo $this->fetch('library/page_footer.lbi'); ?> 
 
<?php echo $this->fetch('library/page_footer_com.lbi'); ?>
</body>
</html>