<?php
$data = array (
  130 => 
  array (
    'cat_id' => '130',
    'cat_name' => '微信营销软件',
    'measure_unit' => '',
    'parent_id' => '0',
    'is_show' => '1',
    'show_in_nav' => '1',
    'grade' => '0',
    'sort_order' => '50',
    'has_children' => '3',
    'goods_num' => 0,
    'level' => 0,
    'id' => '130',
    'name' => '微信营销软件',
  ),
  131 => 
  array (
    'cat_id' => '131',
    'cat_name' => '苹果iOS系统',
    'measure_unit' => '',
    'parent_id' => '130',
    'is_show' => '1',
    'show_in_nav' => '0',
    'grade' => '0',
    'sort_order' => '50',
    'has_children' => '0',
    'goods_num' => '9',
    'level' => 1,
    'id' => '131',
    'name' => '苹果iOS系统',
  ),
  132 => 
  array (
    'cat_id' => '132',
    'cat_name' => '安卓Android系统',
    'measure_unit' => '',
    'parent_id' => '130',
    'is_show' => '1',
    'show_in_nav' => '0',
    'grade' => '0',
    'sort_order' => '50',
    'has_children' => '0',
    'goods_num' => 2,
    'level' => 1,
    'id' => '132',
    'name' => '安卓Android系统',
  ),
  136 => 
  array (
    'cat_id' => '136',
    'cat_name' => 'Windows电脑版',
    'measure_unit' => '',
    'parent_id' => '130',
    'is_show' => '1',
    'show_in_nav' => '0',
    'grade' => '0',
    'sort_order' => '50',
    'has_children' => '0',
    'goods_num' => '1',
    'level' => 1,
    'id' => '136',
    'name' => 'Windows电脑版',
  ),
  137 => 
  array (
    'cat_id' => '137',
    'cat_name' => '男士护理用品',
    'measure_unit' => '',
    'parent_id' => '0',
    'is_show' => '1',
    'show_in_nav' => '0',
    'grade' => '0',
    'sort_order' => '50',
    'has_children' => '0',
    'goods_num' => '3',
    'level' => 0,
    'id' => '137',
    'name' => '男士护理用品',
  ),
  134 => 
  array (
    'cat_id' => '134',
    'cat_name' => '网站系统',
    'measure_unit' => '',
    'parent_id' => '0',
    'is_show' => '1',
    'show_in_nav' => '1',
    'grade' => '0',
    'sort_order' => '50',
    'has_children' => '1',
    'goods_num' => 0,
    'level' => 0,
    'id' => '134',
    'name' => '网站系统',
  ),
  135 => 
  array (
    'cat_id' => '135',
    'cat_name' => 'ecshop模板',
    'measure_unit' => '',
    'parent_id' => '134',
    'is_show' => '1',
    'show_in_nav' => '0',
    'grade' => '0',
    'sort_order' => '50',
    'has_children' => '0',
    'goods_num' => 2,
    'level' => 1,
    'id' => '135',
    'name' => 'ecshop模板',
  ),
  133 => 
  array (
    'cat_id' => '133',
    'cat_name' => '支付测试',
    'measure_unit' => '',
    'parent_id' => '0',
    'is_show' => '1',
    'show_in_nav' => '0',
    'grade' => '0',
    'sort_order' => '50',
    'has_children' => '0',
    'goods_num' => '1',
    'level' => 0,
    'id' => '133',
    'name' => '支付测试',
  ),
);
?>