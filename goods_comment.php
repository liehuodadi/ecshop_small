<?php

/**
 * ECSHOP 提交用户评论
 * ============================================================================
 * * 版权所有 2007-2015 ECSHOP插件网，并保留所有权利。
 * 网站地址: http://www.edait.cn；
 * ----------------------------------------------------------------------------
 * 这不是一个自由软件！您只能在不用于贩卖目的的前提下对程序代码进行修改和
 * 使用；不允许对程序代码以任何形式任何目的的再发布。
 * ============================================================================
 * $Author: ECSHOP插件网 $
 * $Id: goods_comment.php 17217 2015-01-19 06:29:08Z ECSHOP插件网 $
*/

define('IN_ECS', true);

require(dirname(__FILE__) . '/includes/init.php');
require(dirname(__FILE__) . '/includes/cls_image.php');
$image = new cls_image($_CFG['bgcolor']);

if (!isset($_POST['cmt_type']) && !isset($_POST['id']))
{
    /* 只有在没有提交评论内容的情况下才跳转 */
    ecs_header("Location: ./\n");
    exit;
}

$user_id = empty($_SESSION['user_id']) ? 0 : $_SESSION['user_id'];
$email = empty($_POST['email']) ? $_SESSION['email'] : trim($_POST['email']);
$user_name = empty($_SESSION['user_name']) ? '' : trim($_SESSION['user_name']);
$email = htmlspecialchars($email);
$user_name = htmlspecialchars($user_name);
$content = empty($_POST['content']) ? '' : htmlspecialchars(trim($_POST['content']));
$type = empty($_POST['cmt_type']) ? 0: intval($_POST['cmt_type']);
$id = empty($_POST['id']) ? 0 : intval($_POST['id']);
$rank = empty($_POST['comment_rank']) ? 5 : intval($_POST['comment_rank']);
$msg_type = empty($_POST['msg_type']) ? 0 : intval($_POST['msg_type']);
$status = 1 - $GLOBALS['_CFG']['comment_check'];
$captcha = empty($_POST['captcha']) ? '' : trim($_POST['captcha']);

if (!is_email($email))
{
    show_message($_LANG['error_email']);
}
$cur_time = gmtime();
if ((intval($_CFG['captcha']) & CAPTCHA_COMMENT) && gd_version() > 0)
{
    /* 检查验证码 */
    include_once('includes/cls_captcha.php');

    $validator = new captcha();
    if (!$validator->check_word($captcha))
    {
        show_message($_LANG['invalid_captcha']);
    }
}
else
{
    /* 没有验证码时，用时间来限制机器人发帖或恶意发评论 */
    if (!isset($_SESSION['send_time']))
    {
        $_SESSION['send_time'] = 0;
    }

    if (($cur_time - $_SESSION['send_time']) < 30) // 小于30秒禁止发评论
    {
        $result['error']   = 1;
        show_message($cmt_spam_warning);
    }
}

$factor = intval($_CFG['comment_factor']);
if ($type == 0)
{
    /* 只有商品才检查评论条件 */
    switch ($factor)
    {
        case COMMENT_LOGIN :
            if ($_SESSION['user_id'] == 0)
            {
                show_message($_LANG['comment_login']);
            }
            break;
        case COMMENT_CUSTOM :
            if ($_SESSION['user_id'] > 0)
            {
                $sql = "SELECT o.order_id FROM " . $ecs->table('order_info') . " AS o ".
                       " WHERE user_id = '" . $_SESSION['user_id'] . "'".
                       " AND o.order_status = '" . OS_CONFIRMED . "' ".
                       " AND (o.pay_status = '" . PS_PAYED . "' OR o.pay_status = '" . PS_PAYING . "') ".
                       " AND (o.shipping_status = '" . SS_SHIPPED . "' OR o.shipping_status = '" . SS_RECEIVED . "') ".
                       " LIMIT 1";
                $tmp = $db->getOne($sql);
                if (empty($tmp))
                {
                    show_message($_LANG['comment_brought']);
                }
            }
            else
            {
                show_message($_LANG['comment_custom']);
            }
            break;

        case COMMENT_BOUGHT :
            if ($_SESSION['user_id'] > 0)
            {
                $sql = "SELECT o.order_id".
                       " FROM " . $ecs->table('order_info'). " AS o, ".
                       $ecs->table('order_goods') . " AS og ".
                       " WHERE o.order_id = og.order_id".
                       " AND o.user_id = '" . $_SESSION['user_id'] . "'".
                       " AND og.goods_id = '" . $id . "'".
                       " AND o.order_status = '" . OS_CONFIRMED . "' ".
                       " AND (o.pay_status = '" . PS_PAYED . "' OR o.pay_status = '" . PS_PAYING . "') ".
                       " AND (o.shipping_status = '" . SS_SHIPPED . "' OR o.shipping_status = '" . SS_RECEIVED . "') ".
                       " LIMIT 1";
                $tmp = $db->getOne($sql);
                if (empty($tmp))
                {
                    show_message($result['message']);
                }
            }
            else
            {
                show_message($_LANG['comment_custom']);
            }
    }

    $upload_size_limit = $GLOBALS['_CFG']['upload_size_limit'] == '-1' ? ini_get('upload_max_filesize') : $GLOBALS['_CFG']['upload_size_limit'];
    $last_char = strtolower($upload_size_limit{strlen($upload_size_limit)-1});
    switch ($last_char)
    {
        case 'm':
            $upload_size_limit *= 1024*1024;
            break;
        case 'k':
            $upload_size_limit *= 1024;
            break;
    }
    $allow_file_types = '|GIF|JPG|PNG|BMP|';

    /* 检查第一张图片 */
    if ((isset($_FILES['comment_img1']['error']) && $_FILES['comment_img1']['error'] == 0) || (!isset($_FILES['comment_img1']['error']) && isset($_FILES['comment_img1']['tmp_name']) && $_FILES['comment_img1']['tmp_name'] != 'none'))
    {
        if ($_FILES['comment_img1']['size'] / 1024 > $upload_size_limit)
        {
            show_message(sprintf($GLOBALS['_LANG']['upload_file_limit'], $upload_size_limit));
            exit;
        }

        // 检查文件格式
        if (!check_file_type($_FILES['comment_img1']['tmp_name'], $_FILES['comment_img1']['name'], $allow_file_types))
        {
            show_message("只能上传GIF,JPG,PNG,BMP的图片！");
        }
    }

    /* 检查第二张图片 */
    if ((isset($_FILES['comment_img2']['error']) && $_FILES['comment_img2']['error'] == 0) || (!isset($_FILES['comment_img2']['error']) && isset($_FILES['comment_img2']['tmp_name']) && $_FILES['comment_img2']['tmp_name'] != 'none'))
    {
        if ($_FILES['comment_img2']['size'] / 1024 > $upload_size_limit)
        {
            show_message(sprintf($GLOBALS['_LANG']['upload_file_limit'], $upload_size_limit));
            exit;
        }

        // 检查文件格式
        if (!check_file_type($_FILES['comment_img2']['tmp_name'], $_FILES['comment_img2']['name'], $allow_file_types))
        {
            show_message("只能上传GIF,JPG,PNG,BMP的图片！");
        }
    }

    /* 检查第三张图片 */
    if ((isset($_FILES['comment_img3']['error']) && $_FILES['comment_img3']['error'] == 0) || (!isset($_FILES['comment_img3']['error']) && isset($_FILES['comment_img3']['tmp_name']) && $_FILES['comment_img3']['tmp_name'] != 'none'))
    {
        if ($_FILES['comment_img3']['size'] / 1024 > $upload_size_limit)
        {
            show_message(sprintf($GLOBALS['_LANG']['upload_file_limit'], $upload_size_limit));
            exit;
        }

        // 检查文件格式
        if (!check_file_type($_FILES['comment_img3']['tmp_name'], $_FILES['comment_img3']['name'], $allow_file_types))
        {
            show_message("只能上传GIF,JPG,PNG,BMP的图片！");
        }
    }

    /* 检查第四张图片 */
    if ((isset($_FILES['comment_img4']['error']) && $_FILES['comment_img4']['error'] == 0) || (!isset($_FILES['comment_img4']['error']) && isset($_FILES['comment_img4']['tmp_name']) && $_FILES['comment_img4']['tmp_name'] != 'none'))
    {
        if ($_FILES['comment_img4']['size'] / 1024 > $upload_size_limit)
        {
            show_message(sprintf($GLOBALS['_LANG']['upload_file_limit'], $upload_size_limit));
            exit;
        }

        // 检查文件格式
        if (!check_file_type($_FILES['comment_img4']['tmp_name'], $_FILES['comment_img4']['name'], $allow_file_types))
        {
            show_message("只能上传GIF,JPG,PNG,BMP的图片！");
        }
    }

    /* 检查第四张图片 */
    if ((isset($_FILES['comment_img5']['error']) && $_FILES['comment_img5']['error'] == 0) || (!isset($_FILES['comment_img5']['error']) && isset($_FILES['comment_img5']['tmp_name']) && $_FILES['comment_img5']['tmp_name'] != 'none'))
    {
        if ($_FILES['comment_img5']['size'] / 1024 > $upload_size_limit)
        {
            show_message(sprintf($GLOBALS['_LANG']['upload_file_limit'], $upload_size_limit));
            exit;
        }

        // 检查文件格式
        if (!check_file_type($_FILES['comment_img5']['tmp_name'], $_FILES['comment_img5']['name'], $allow_file_types))
        {
            show_message("只能上传GIF,JPG,PNG,BMP的图片！");
        }
    }

    $photo1 = upload_file($_FILES['comment_img1'], 'feedbackimg');
    $photo2 = upload_file($_FILES['comment_img2'], 'feedbackimg');
    $photo3 = upload_file($_FILES['comment_img3'], 'feedbackimg');
    $photo4 = upload_file($_FILES['comment_img4'], 'feedbackimg');
    $photo5 = upload_file($_FILES['comment_img5'], 'feedbackimg');
	  
    $photo1_thumb = '';
    $photo2_thumb = '';
    $photo3_thumb = '';
    $photo4_thumb = '';
    $photo5_thumb = '';

    for ($i=1; $i<=5; $i++)
    {
        $img_name_temp = 'photo'.$i;
        $img_name = $$img_name_temp;
        if (!empty($img_name))
        {
            $ext = end(explode('.', $img_name));
            $filename = str_replace('.'.$ext, '', $img_name);
            $tmp = DATA_DIR . '/feedbackimg/'.$img_name;
            $image->make_thumb1($tmp, 500, 500, DATA_DIR.'/feedbackimg/', $filename, $ext);
        }
    }

    for ($i=1; $i<=5; $i++)
    {
        $img_name_temp = 'photo'.$i;
        $img_name = $$img_name_temp;
        if (!empty($img_name))
        {
            $ext = end(explode('.', $img_name));
            $filename = str_replace('.'.$ext, '', $img_name).'_thumb';
            $tmp = DATA_DIR . '/feedbackimg/'.$img_name;
            $name_pri = 'photo'.$i.'_thumb';
            $res = $image->make_thumb1($tmp, 130, 130, DATA_DIR.'/feedbackimg/', $filename, $ext);
            if (!empty($res))
            {
                $$name_pri = $filename.'.'.$ext;
            }
            else
            {
                $$name_pri = $img_name;
            }
        }
    }

    /* 保存评论内容 */
    $sql = "INSERT INTO " .$GLOBALS['ecs']->table('comment') .
           "(comment_type, id_value, email, user_name, content, comment_rank, add_time, ip_address, status, parent_id, user_id, photo1, photo2, photo3, photo4, photo5, photo1_thumb, photo2_thumb, photo3_thumb, photo4_thumb, photo5_thumb) VALUES " .
           "('" .$type. "', '" .$id. "', '$email', '$user_name', '" .$content."', '".$rank."', ".gmtime().", '".real_ip()."', '$status', '0', '$user_id', '$photo1', '$photo2', '$photo3', '$photo4', '$photo5', '$photo1_thumb', '$photo2_thumb', '$photo3_thumb', '$photo4_thumb', '$photo5_thumb')";
    if ($GLOBALS['db']->query($sql))
    {
        $message = $_CFG['comment_check'] ? $_LANG['cmt_submit_wait'] : $_LANG['cmt_submit_done'];
        $url = build_uri('goods', array('gid' => $id));
        show_message($message, "返回商品详情", "$url", 'error');
    }
    $_SESSION['send_time'] = $cur_time;
}

?>