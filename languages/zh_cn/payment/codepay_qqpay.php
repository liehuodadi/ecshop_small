<?php
/**
 * 码支付支付宝接口
 * ============================================================================
 * 版权所有 2005-2017 码支付保留所有权利。
 * 网站地址: http://codepay.fateqq.com
 * ----------------------------------------------------------------------------
 * 这是一个由码支付提供的支付宝 ecshop支付插件
 * 申请开通：https://codepay.fateqq.com/reg.html
 * 免签约 免手续费 及时到账
 * ============================================================================
 * $Author: codepay $
 */
global $_LANG;
$_LANG['codepay_qqpay'] = '<font color="#FF0000">QQ钱包支付</font>';
$_LANG['codepay_qqpay_desc'] = 'QQ钱包(https://qpay.qq.com/)是使用QQ直接快捷付款。由码支付提供接口；个人站长或不能申请QQ支付接口的企业建议使用，使用个人QQ帐号，完全即时到账！支持返回订单支付状态，无需手续费，不用代收，客户购买商品支付款项直接打入您的个人QQ钱包账户。<a href="https://codepay.fateqq.com/i/17337" target="_blank">点击注册账户</a>';
$_LANG['codepay_id'] = '码支付ID';
$_LANG['codepay_return'] = '通知地址';
$_LANG['codepay_return_desc'] = '<font color="#FF0000">注意：该地址填写到<a href="https://codepay.fateqq.com/down.html" target="_blank">软件设置</a>或者 <a href="http://codepay.fateqq.com:52888/admin/#/dataSet.html" target="_blank">云端设置</a>中</font><br>地址为：您的域名/respond.php（例如：http://www.asneel.com/respond.php）';
$_LANG['codepay_key'] = '通信密钥';
$_LANG['codepay_key_desc'] = '从码支付系统设置 <a href="https://codepay.fateqq.com/i/17337" target="_blank">获取密钥</a> 填入';
$_LANG['codepay_id_desc'] = '从码支付系统设置 <a href="https://codepay.fateqq.com/i/17337" target="_blank"><font color="#FF0000">获取ID</font></a> (注册或登录账户在系统设置里面获得) 填入';
$_LANG['pay_button'] = '立即支付';
$_LANG['codepay_act'] = '选择接口类型';
$_LANG['codepay_act_desc'] = '软件版需<a href="https://codepay.fateqq.com/down.html" target="_blank">运行软件</a> <a href="http://codepay.fateqq.com:52888/admin/#/vip.html" target="_blank">更多详情</a>';
$_LANG['codepay_act_range'][0] = '软件版';
$_LANG['codepay_act_range'][1] = '认证版';
?>